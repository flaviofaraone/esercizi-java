package it.softwareinside.Flavio.esercizioOmbrelloni.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import it.softwareinside.Flavio.esercizioOmbrelloni.model.Ombrellone;
import it.softwareinside.Flavio.esercizioOmbrelloni.service.OmbrelloneService;

@org.springframework.stereotype.Controller
public class Controller {

	@Autowired
	private OmbrelloneService ombrelloneService;
	
	@GetMapping(value="/")
	public ModelAndView getHome() {
		ModelAndView model =new ModelAndView();
		model.setViewName("index");
		
		model.addObject("ombrellone" , new Ombrellone());
		model.addObject("listaOmbrelloniDisponibili" 
							, ombrelloneService.getAllombrelloniAvalaible());
		
		return model;
		
	}
	
	@PostMapping(value="crea-ombrellone")
	public String aggiungiombrellone(@ModelAttribute Ombrellone ombrellone) {
		
		ombrelloneService.aggiungiOmbrellone(ombrellone);
		
		return "redirect:/";
	}
	
	
	@GetMapping(value="cancella-ombrellone")
	public String rimuoviOmbrellone(@RequestParam(name="id") Integer id) {
		
		ombrelloneService.rimuoviOmbrellone(id);
		
		return "redirect:/";
	}
	
}