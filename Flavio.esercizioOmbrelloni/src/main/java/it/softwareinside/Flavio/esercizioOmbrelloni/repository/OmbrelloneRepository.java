package it.softwareinside.Flavio.esercizioOmbrelloni.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import it.softwareinside.Flavio.esercizioOmbrelloni.model.Ombrellone;

public interface OmbrelloneRepository extends CrudRepository <Ombrellone, Integer> {

	@Query(" select c from Ombrelloni c where c.isAvailable = true")
	ArrayList<Ombrellone> findOmbrelloniDisponibili();
	
	@Query(" select sum() as prezzo from Ombrelloni")
	Double sommaTuttiValori();
	
	@Query(" select sum() as prezzo from Ombrelloni c where c where c.isAvailable = true")
	Double sommaValoriLiberi();
}
