package it.softwareinside.Flavio.esercizioOmbrelloni.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import it.softwareinside.Flavio.esercizioOmbrelloni.model.Ombrellone;
import it.softwareinside.Flavio.esercizioOmbrelloni.repository.OmbrelloneRepository;


@Service
public class OmbrelloneService {

	@Autowired
	private OmbrelloneRepository ombrelloneRepository;
	
	public List<Ombrellone> getAllombrelloniAvalaible(){
		return ombrelloneRepository.findOmbrelloniDisponibili();
	}
	
	public void aggiungiOmbrellone(Ombrellone ombrellone) {
		ombrelloneRepository.save(ombrellone);
	}
	
	public void rimuoviOmbrellone(int id) {
		ombrelloneRepository.deleteById(id);
	}
	
}