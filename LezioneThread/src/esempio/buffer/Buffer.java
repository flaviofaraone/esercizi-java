package esempio.buffer;

public class Buffer {

	private int data[];
	private int numero_elementi =10;
	private boolean isEmpty = true;
	
	
	
	public Buffer() {
		data = new int[numero_elementi];
	}
	
	
	public int getNumero_elementi() {
		return numero_elementi;
	}
	
	
	public synchronized void write(int val) throws InterruptedException {
		
		
		//ASPETTO CHE IL CONSUMER LEGGA IL DATO..
		while(!isEmpty) {
			System.out.println(Thread.currentThread().getName() 
					+ "attende che il consumer legga il dato");
			wait();
		}
		
		
		data[val] = val; //scrivo il dato
		isEmpty = false;
		
		System.out.println("SCRITTURA DEL DATO pos " + val + "data"  );
		notify();
	}
	
	
	
	public synchronized int read(int index) throws InterruptedException {
		
		while(isEmpty) {
			
			System.out.println("who i'm ?");
			wait();
		}
		
		isEmpty = true;
		
		System.out.println("lettura " + data[index]);
		notify();
		return data[index];
	}
	
	
	
	
}
