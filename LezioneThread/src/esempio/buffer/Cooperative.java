package esempio.buffer;

public class Cooperative {

	public static void main(String[] args) throws InterruptedException {
		
		Buffer buffer = new Buffer();
		
		RunProducer producer = new RunProducer(buffer);
		RunConsumer consumer = new RunConsumer(buffer);
		
		
		Thread t1 = new Thread(producer);
		Thread t2 = new  Thread(consumer);
		
		t1.start();
		t2.start();
		
		
		t1.join();
		t2.join();
		
		System.out.println("Fine anche del main ...");

	}

}
