package esempio.buffer;

import java.util.Random;

public class RunConsumer implements Runnable{

	
	private Buffer buffer;
	
	public RunConsumer(Buffer buffer) {
		
		this.buffer = buffer;
	}
	
	@Override
	public void run() {
		
	
		for (int i = 0; i < buffer.getNumero_elementi(); i++) {
			Random rand = new Random();
			int riposati = rand.nextInt(1000 * 5);
			
			try {
				Thread.sleep(riposati);
				buffer.read(i);
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		}
		
		
	}

	
	
}
