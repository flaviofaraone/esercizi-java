package esempioCreazioneThread;

import java.util.Random;

public class Cavallo implements Runnable {

	private String nome;
	
	
	private static boolean race = true;
	private static String  vincitore = "";
	
	
	public static String getVincitore() {
		return vincitore;
	}
	
	
	public Cavallo(String nome) {
		this.nome = nome;
	}
	
	//  %(2)
	//  %(4) ---->  -- -- - - -- - -
	//
	
	@Override
	public void run() {
		
		Random rand = new Random();
		
		for(int i = 0; i < 7 && race ; i++) {
			
			try {
				Thread.sleep(1000 * (rand.nextInt(3) + 1)   );
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			System.out.println(nome + " sta correndo " + (10 - i) + " metri mancanti"  );
			
			if(i == 6  && race) {
				race = false;
				vincitore = nome;
			}
			
		}
		
		System.err.println(nome + " ha terminato la gara");
	
	}

	//equq
	
}
