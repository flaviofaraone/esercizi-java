package esempioCreazioneThread;

public class MainTest {

	
	public static void main(String[] args) throws InterruptedException {
		
		
		System.out.println("Is1");
		System.out.println("Is2");
		
		
		Task task = new Task(); // RUNNABLE
		task.setTempoSleep(8); //
		Thread t1 = new Thread(task , "lumaca");
		
		
		Task task2 = new Task();
		task2.setTempoSleep(2);
		
		Thread t2 = new Thread(task2 , "volpe");
		
		
		t1.start();
		t2.start();
		
		t2.join(); // --------------------- //
		
		
		//FOR DEL MAIN....
		for(int i =0 ; i < 10 ; i++) {
			
			Thread.sleep(1000 * 4);
			System.out.println("SONO IL MAIN " + i);
		}
		
		
		
	}
	
	
	
	
}
