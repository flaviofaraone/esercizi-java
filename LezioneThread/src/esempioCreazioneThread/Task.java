package esempioCreazioneThread;

public class Task implements Runnable{

	private int tempoSleep; // in secondi
	
	
	public void setTempoSleep(int tempoSleep) {
		this.tempoSleep = tempoSleep;
	}
	
	
	@Override
	public void run() {
		
		for(int i = 0 ; i < 10 ; i++) {
			System.out.println("Sono " + Thread.currentThread().getName() 
								+" STAMPA " + i);
			
			try {
				Thread.sleep(1000 * tempoSleep); 
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		
		}
		
	}

	
	
	
	
}
