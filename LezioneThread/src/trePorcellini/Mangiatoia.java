package trePorcellini;

public class Mangiatoia {

	
	public static void main(String[] args) throws InterruptedException {
		
		
		Pig pig1 = new Pig("porcellino1");
		Pig pig2 = new Pig("porcellino2");
		Pig pig3 = new Pig("porcellino3");

		
		Thread tpig1 = new Thread(pig1);
		Thread tpig2 = new Thread(pig2);
		Thread tpig3 = new Thread(pig3);

		tpig1.start();
		tpig2.start();
		tpig3.start();
		
		tpig1.join();
		
		System.out.println(pig1.getNome() + "  " + pig1.getQuantita());
		System.out.println(pig2.getNome() + "  " + pig2.getQuantita());
		System.out.println(pig3.getNome() + "  " + pig3.getQuantita());

		
	}
	
	
	
	
}
