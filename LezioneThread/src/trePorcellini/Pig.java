package trePorcellini;

import java.util.Random;

public class Pig implements Runnable {

	private static volatile int ciboDisponibile = 15;
	private String nome;
	private int quantita;
	
	
	
	public Pig(String nome) {
		super();
		this.nome = nome;
	}


	public int getQuantita() {
		return quantita;
	}
	
	
	public String getNome() {
		return nome;
	}
	

	@Override
	public void run() {
		
		Random random = new Random();
		
		while(ciboDisponibile > 0) {
			
			try {
				
				int mangia =  random.nextInt(3);
				System.out.println("STA MANGIANDO IL PORCELLINO " + nome);
				if(ciboDisponibile - mangia >= 0) {
					ciboDisponibile = ciboDisponibile -mangia;
					quantita += mangia;
				}
				
				System.out.println("RIMANENZA " + ciboDisponibile);
				Thread.sleep(1000 * (random.nextInt(6) + 1));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
		}
		
	}
	

}
