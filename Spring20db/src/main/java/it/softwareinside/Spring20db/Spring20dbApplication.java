package it.softwareinside.Spring20db;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Spring20dbApplication {

	public static void main(String[] args) {
		SpringApplication.run(Spring20dbApplication.class, args);
	}

}
