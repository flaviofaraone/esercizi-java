package it.softwareinside.Spring20db.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import it.softwareinside.Spring20db.model.Persona;
import it.softwareinside.Spring20db.repository.PersonaRepository;

@org.springframework.stereotype.Controller
public class Controller {
	
	
	@Autowired
	private PersonaRepository personaRepository;
	
	
//	@GetMapping(value="/")
//	public ModelAndView home() {
//		ModelAndView model =new ModelAndView();
//		model.setViewName("index");
//		
//		
//		Persona persona = new Persona("fla", "faraone", 25, "389654");
//		
//		personaRepository.save(persona);
//		
//		return model;
//	}
	
	@GetMapping(value="/")
	public ModelAndView getHome() {
		ModelAndView model =new ModelAndView();
		model.setViewName("index");

		model.addObject("persona" , new Persona()) ;
		return model;

	}
	
	
	@PostMapping(value="/postPersona")
	public String Postpersona(@ModelAttribute Persona persona) {
		
		personaRepository.save(persona);
		
		return "redirect:/";
	}
}
