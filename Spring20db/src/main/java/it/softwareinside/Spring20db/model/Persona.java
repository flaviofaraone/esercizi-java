package it.softwareinside.Spring20db.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor

@Entity
@Table(name="persone")
public class Persona {
	
	public Persona(String nome, String cognome, int eta, String phone) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.eta = eta;
		this.phone = phone;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private int id;
	
	@Column(name="nome")
	private String nome;
	
	@Column(name="cognome")
	private String cognome;
	
	@Column(name="eta")
	private int eta;
	
	@Column(name="phone")
	private String phone;
}
