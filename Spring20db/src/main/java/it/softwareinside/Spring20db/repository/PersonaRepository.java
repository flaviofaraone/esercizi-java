package it.softwareinside.Spring20db.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.softwareinside.Spring20db.model.Persona;

@Repository
public interface PersonaRepository extends CrudRepository<Persona, Integer>  {

}
