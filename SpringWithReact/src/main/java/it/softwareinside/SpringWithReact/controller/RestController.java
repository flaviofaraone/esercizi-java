package it.softwareinside.SpringWithReact.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import it.softwareinside.SpringWithReact.model.Team;
import it.softwareinside.SpringWithReact.service.TeamService;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api")
public class RestController {
	@Autowired
	private TeamService teamService;
	
	@GetMapping("/teams")
	public Iterable<Team> getTeams(){
		return teamService.findAll();
	}
	
	@GetMapping("/teams-by-ranking/{limit}")
	public List<Team> getTeamsByRanking(@PathVariable int limit){
		return teamService.findByRanking(limit);
	}
	
	@GetMapping("/teams-by-ranking/{min}/{max}")
	public List<Team> getTeamsByRanking(@PathVariable int min, @PathVariable int max){
		return teamService.findByRanking(min, max);
	}
	
	@PostMapping("/add-team")
	public void addTeam(@RequestBody Team team) {
		
		teamService.save(team);
		
	}
}
