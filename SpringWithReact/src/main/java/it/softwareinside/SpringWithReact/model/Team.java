package it.softwareinside.SpringWithReact.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="teams")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Team {
	
	@Id
	@Column(name = "name", length = 64)
	private String name;
	
	@Column(name = "ranking")
	private int ranking;
}
