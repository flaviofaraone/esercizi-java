package it.softwareinside.SpringWithReact.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import it.softwareinside.SpringWithReact.model.Team;

@Repository
public interface TeamRepository extends CrudRepository<Team, String>{

	@Query("select t from Team t where t.ranking > ?1")
	List<Team> findByRanking(int limit);
	
	@Query("select t from Team t where t.ranking between ?1 and ?2")
	List<Team> findByRanking(int min, int max);
}
