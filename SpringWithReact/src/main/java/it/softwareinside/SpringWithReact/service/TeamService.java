package it.softwareinside.SpringWithReact.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.softwareinside.SpringWithReact.model.Team;
import it.softwareinside.SpringWithReact.repository.TeamRepository;

@Service
public class TeamService {
	@Autowired
	private TeamRepository teamRepository;
	
	public void save(Team team) {
		teamRepository.save(team);
	}
	
	public Iterable<Team> findAll(){
		return teamRepository.findAll();
	}
	
	public List<Team> findByRanking(int limit){
		return teamRepository.findByRanking(limit);
	}
	
	public List<Team> findByRanking(int min, int max){
		return teamRepository.findByRanking(min, max);
	}
}
