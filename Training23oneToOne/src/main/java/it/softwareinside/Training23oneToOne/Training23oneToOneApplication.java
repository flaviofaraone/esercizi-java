package it.softwareinside.Training23oneToOne;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Training23oneToOneApplication {

	public static void main(String[] args) {
		SpringApplication.run(Training23oneToOneApplication.class, args);
	}

}
