package it.softwareinside.Training23oneToOne.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import it.softwareinside.Training23oneToOne.model.Computer;
import it.softwareinside.Training23oneToOne.service.ComputerService;

@org.springframework.stereotype.Controller
public class Controller {

	@Autowired
	private ComputerService computerService;
	
	@GetMapping(value="/")
	public ModelAndView getHome() {
		ModelAndView model =new ModelAndView();
		model.setViewName("index");
		
		model.addObject("computer" , new Computer());
		
//		model.addObject("computerByNome" , computerService.findByMarca("acer"));
	
		return model;
	}
	
	@GetMapping(value="crea-computer")
	public String creaComputer() {
		
//		computerService.creaRandomPc();
		
		return "redirect:/";
	}
	
}
