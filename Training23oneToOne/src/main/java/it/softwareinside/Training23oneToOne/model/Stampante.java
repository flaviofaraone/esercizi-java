package it.softwareinside.Training23oneToOne.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@Entity
@Table(name="stampante")
public class Stampante {
	
	
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name="marca")
	private String marca;

	@Column(name="prezzo")
	private double prezzo;

	public Stampante(String marca, double prezzo) {
		super();
		this.marca = marca;
		this.prezzo = prezzo;
	}

}
