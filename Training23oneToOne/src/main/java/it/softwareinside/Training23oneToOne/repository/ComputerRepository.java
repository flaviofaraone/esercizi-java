package it.softwareinside.Training23oneToOne.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.softwareinside.Training23oneToOne.model.Computer;

@Repository
public interface ComputerRepository extends CrudRepository<Computer, Integer> {

	@Query("select x from Computer x where x.marca like ?1")
	List<Computer> findByMarca(String marca);
	
	@Query("select x from Computer x where x.prezzo == ?1")
	List<Computer> findByPrezzo(double prezzo);
	
	@Query("select x from Computer x where x.prezzo >= ?1 and x.prezzo <= ?2")
	List<Computer> findBetweenPrezzo(double min , double max);
}
