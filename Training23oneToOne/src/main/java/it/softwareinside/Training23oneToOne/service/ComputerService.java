package it.softwareinside.Training23oneToOne.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.softwareinside.Training23oneToOne.model.Computer;
import it.softwareinside.Training23oneToOne.repository.ComputerRepository;

@Service
public class ComputerService {
	
	@Autowired
	private ComputerRepository computerRepository;
	
	public void addComputer(Computer computer) {
		computerRepository.save(computer);
	}
	
	public void removeComputer(Computer computer) {
		computerRepository.delete(computer);
	}
	
	public void removeIdComputer(int id) {
		computerRepository.deleteById(id);
	}
	
	public List<Computer> findByMarca(String marca){
		return computerRepository.findByMarca(marca);
	}
	
	public List<Computer> findByPrezzo(Double prezzo){
		return computerRepository.findByPrezzo(prezzo);
	}
	
	public List<Computer> findBetweenPrezzo(int min , int max){
		return computerRepository.findBetweenPrezzo( min ,  max);
	}
}
