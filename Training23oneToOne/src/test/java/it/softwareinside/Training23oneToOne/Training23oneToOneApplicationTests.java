package it.softwareinside.Training23oneToOne;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import it.softwareinside.Training23oneToOne.model.Computer;
import it.softwareinside.Training23oneToOne.repository.ComputerRepository;

@SpringBootTest
class Training23oneToOneApplicationTests {
	
	@Autowired
	private ComputerRepository ComputerRepository;
	
	@Test
	void contextLoads() {
		
		Computer computer = (Computer) ComputerRepository.findByMarca("acer");
		
		assertEquals(computer.getMarca(), "acer");
		assertEquals(computer.getId() , 1);
		assertEquals(computer.getPrezzo() , 300.0);
	}

}
