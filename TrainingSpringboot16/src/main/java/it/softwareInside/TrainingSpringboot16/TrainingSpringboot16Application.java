package it.softwareInside.TrainingSpringboot16;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainingSpringboot16Application {

	public static void main(String[] args) {
		SpringApplication.run(TrainingSpringboot16Application.class, args);
	}

}
