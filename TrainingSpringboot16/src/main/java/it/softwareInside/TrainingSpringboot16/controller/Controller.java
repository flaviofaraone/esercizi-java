package it.softwareInside.TrainingSpringboot16.controller;

import java.util.Calendar;
import java.util.Date;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@org.springframework.stereotype.Controller
public class Controller {
	
	@GetMapping(value = "/")
	public ModelAndView homePage() {
		ModelAndView model = new ModelAndView();
		model.setViewName("index");
		
		return model;
	}
	
	@GetMapping (value = {"/contatti","/info"})
	public ModelAndView contatti() {
		ModelAndView model = new ModelAndView();
		model.setViewName("contatti");
		
		return model;
	}
	
	@GetMapping (value = "/example")
	public ModelAndView info() {
		
		ModelAndView model = new ModelAndView();
		
		String valorePassato = "Ciao a tutti passo un dato da Java";
		model.addObject("message", valorePassato);

		model.setViewName("prova");
		return model;
	}
	
	@GetMapping (value = "/orario")
	public ModelAndView orario() {
		
		ModelAndView model = new ModelAndView();
		
		Date valorePassato = Calendar.getInstance().getTime() ;
		model.addObject("messageHour", valorePassato);

		model.setViewName("orario");
		return model;
	}
	
	
	@GetMapping (value = "/game")
	public ModelAndView game() {
		
		ModelAndView model = new ModelAndView();
		
		int random_int = (int)Math.floor(Math.random()*(37)+0);
		model.addObject("messageGame", random_int);

		model.setViewName("game");
		return model;
	}
	
	
}
