package it.softwareInside.apiFinanceTrain16;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiFinanceTrain16Application {

	public static void main(String[] args) {
		SpringApplication.run(ApiFinanceTrain16Application.class, args);
	}

}
