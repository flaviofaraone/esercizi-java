package it.softwareInside.apiFinanceTrain16.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import it.softwareInside.apiFinanceTrain16.model.Dat;
import it.softwareInside.apiFinanceTrain16.service.DatService;

@org.springframework.stereotype.Controller
public class Controller {
	
	@Autowired
	private DatService datService;
	
	@GetMapping(value="/")
	public ModelAndView model() {
		ModelAndView model = new ModelAndView();
		model.setViewName("index");
		
		return model;
	}
	
	@GetMapping(value="/dati")
	public ModelAndView dati(@RequestParam(name="id") String id) {		
		ModelAndView model = new ModelAndView();
		model.setViewName("dati");	
		Dat dat = datService.getDat(id);
		model.addObject("id",dat);
		
		System.out.println(model);
		
		return model;
	}
}