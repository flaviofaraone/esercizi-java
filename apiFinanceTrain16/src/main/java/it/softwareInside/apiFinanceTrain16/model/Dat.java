package it.softwareInside.apiFinanceTrain16.model;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Dat {
	private ArrayList<Crypto> crypto;
	
}
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
class Crypto{
	private String id  , symbol , name , explorer;
	private Double rank , changePercent24Hr , priceUsd;	
}

/**
 *  "id": "bitcoin",
      "rank": "1",
      "symbol": "BTC",
      "name": "Bitcoin",
      "supply": "18729975.0000000000000000",
      "maxSupply": "21000000.0000000000000000",
      "marketCapUsd": "615201490075.1644267921891500",
      "volumeUsd24Hr": "28518127642.6016648573648541",
      "priceUsd": "32845.8254789536252340",
      "changePercent24Hr": "-10.2944957904971298",
      "vwap24Hr": "34004.4585805702568580",
      "explorer": "https://blockchain.info/"
 * 
 */