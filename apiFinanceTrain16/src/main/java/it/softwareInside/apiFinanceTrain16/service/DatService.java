package it.softwareInside.apiFinanceTrain16.service;
import java.util.ArrayList;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import it.softwareInside.apiFinanceTrain16.model.Dat;

@Service
public class DatService {
	
	private final String URL ="https://api.coincap.io/v2/assets/";
		
	public Dat  getDat(String id) {
		RestTemplate restTemplate = new RestTemplate();
		Dat dat = restTemplate.getForObject(URL + id, Dat.class);
		
		return dat;
	}
}