package it.softwareInside.apiPersone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiPersoneApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiPersoneApplication.class, args);
	}

}
