package it.softwareInside.apiPersone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import it.softwareInside.apiPersone.model.Results;
import it.softwareInside.apiPersone.model.Weather;
import it.softwareInside.apiPersone.service.WeatherService;

@org.springframework.stereotype.Controller
public class Controller {
	
	
	//il metodo di sotto è meglio.
	
	@GetMapping(value="/")
	public ModelAndView model() {
		ModelAndView model = new ModelAndView();
		model.setViewName("index");
		RestTemplate restTemplate = new RestTemplate();
		
		Results results = restTemplate.getForObject("https://randomuser.me/api/", Results.class);
		
		System.out.println(results.getResults().get(0));
				
		String nome = results.getResults().get(0).getName().getFirst();
		
		System.out.println(nome);
		
//front-end		model.addObject("user" , results);
		
		return model;
	}
	
	
	
//	@GetMapping(value="/indexWeather")
//	public ModelAndView modelWeather() {
//		ModelAndView model = new ModelAndView();
//		model.setViewName("index");
//		RestTemplate restTemplate = new RestTemplate();
//	
//		Weather weather = restTemplate.getForObject("https://goweather.herokuapp.com/weather/bari", Weather.class);
//		
//		System.out.println(weather);
//	
//		return model;
//
//	}
	
	@Autowired
	private WeatherService weatherService;
	
	@GetMapping(value="/indexWeather")
	public ModelAndView modelWeather() {
		ModelAndView model = new ModelAndView();
		model.setViewName("indexWeather");
		
		Weather weather= weatherService.getWeather("bari");
		System.out.println(weather);
		
		return model;
	}
	
	
	
	
	
	
}
