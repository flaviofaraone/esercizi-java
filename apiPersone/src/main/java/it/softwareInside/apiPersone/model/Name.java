package it.softwareInside.apiPersone.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Name {
	
	private String title;
	
	private String first;
	
	private String last;
	
	
	
}
