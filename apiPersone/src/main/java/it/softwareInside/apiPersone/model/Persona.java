package it.softwareInside.apiPersone.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Persona {
	
	private String gender;
	
	private Name name;
	
}
