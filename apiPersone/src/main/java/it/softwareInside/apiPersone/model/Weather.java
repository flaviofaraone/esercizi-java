package it.softwareInside.apiPersone.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Weather {
	
	private String temperature , wind , description;
	private ArrayList<Forecast> forecast;
	
}
