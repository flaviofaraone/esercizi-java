package it.softwareInside.apiPersone.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import it.softwareInside.apiPersone.model.Weather;

@Service
public class WeatherService {
	
	
	private final String URL ="https://goweather.herokuapp.com/weather/";
	
	public Weather getWeather(String city) {
		RestTemplate restTemplate =new RestTemplate();
		
		Weather weather = restTemplate.getForObject(URL + city, Weather.class);
		
		return weather;
	}

}
