package it.softwareInside.bancaInvestitoreAssets;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancaInvestitoreAssetsApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancaInvestitoreAssetsApplication.class, args);
	}

}
