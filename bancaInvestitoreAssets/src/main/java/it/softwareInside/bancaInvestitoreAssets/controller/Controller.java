package it.softwareInside.bancaInvestitoreAssets.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import it.softwareInside.bancaInvestitoreAssets.model.Asset;
import it.softwareInside.bancaInvestitoreAssets.model.Investitore;
import it.softwareInside.bancaInvestitoreAssets.service.InvestitoreService;

@org.springframework.stereotype.Controller
public class Controller {
	
	@Autowired
	private InvestitoreService investitoreService;
	
	@GetMapping(value="/")
	public ModelAndView index() {
		ModelAndView model =new ModelAndView();
		model.setViewName("index");
		
		model.addObject("investitori" , investitoreService.getAllInvestitori());
						
		return model;
	}
	
	@GetMapping(value="/creaInvestitore")
	public String professionista() {
		
		investitoreService.creaInvestitore();
		
		return"redirect:/";
	}
	
	@PostMapping(value="postInvestitore")
	public ModelAndView postInvestitore(@ModelAttribute Investitore investitore,
											@RequestParam(name="prezzi") double[] prezzi ,
											@RequestParam(name="azione") String[] nomi)throws Exception{
		
		ArrayList<Asset> vett = new ArrayList<>();
		
		if (prezzi.length != nomi.length)
			throw new Exception("invalid array size");
		
		for(int i= 0 ; i<prezzi.length ; i++) {
			vett.add(new Asset(nomi[i] , prezzi[i]));
		}
		
		
		
		
		return null;
	}
	
	
	
}
