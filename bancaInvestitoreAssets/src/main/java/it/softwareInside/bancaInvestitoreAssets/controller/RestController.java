package it.softwareInside.bancaInvestitoreAssets.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import it.softwareInside.bancaInvestitoreAssets.model.Investitore;
import it.softwareInside.bancaInvestitoreAssets.service.InvestitoreService;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api")
public class RestController {
	
	@Autowired
	private InvestitoreService investitoreService;
	
	@GetMapping(value="/getAllInvestitori")
	public List<Investitore> getAllInvestitori() {
		
		return investitoreService.getAllInvestitori();		
	}
	
}
