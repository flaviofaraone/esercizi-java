package it.softwareInside.bancaInvestitoreAssets.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name="asset")
public class Asset {

	@Id
	@GeneratedValue(strategy =GenerationType.AUTO )
	@Column(name="idAsset")
	private int idAsset;
	
	@Column(name="nome")
	private String nome;
	
	@Column(name="prezzo")
	private double prezzo;

	public Asset(String nome, double prezzo) {
		super();
		this.nome = nome;
		this.prezzo = prezzo;
	}
	
}