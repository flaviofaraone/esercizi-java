package it.softwareInside.bancaInvestitoreAssets.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name="banca")
public class Banca {
	
	@Id
	@Column(name="nomeBanca" , length =5)
	private String nomeBanca ;
	
	@Column(name="tipologia")
	private String tipologia;
	
	@Column(name="sede")
	private String sede;

	public Banca(String nomeBanca, String tipologia, String sede) {
		super();
		this.nomeBanca = nomeBanca;
		this.tipologia = tipologia;
		this.sede = sede;
	}

	
}