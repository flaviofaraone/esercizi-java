package it.softwareInside.bancaInvestitoreAssets.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Candels{
	
	private int InstrumentID;
	private String FromDate;
	private double Open;
	private double Low;
	private double Close;
	private double High;
}