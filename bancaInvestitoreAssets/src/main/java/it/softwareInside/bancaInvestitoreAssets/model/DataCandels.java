package it.softwareInside.bancaInvestitoreAssets.model;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataCandels {
	
	private String InstrumentId;
	private List<Candels> candels;
	
}