package it.softwareInside.bancaInvestitoreAssets.model;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name="investitore")
public class Investitore {

	@Id
	@Column(name="codiceFiscale" ,length =10)
	private String codiceFiscale;
	
	@Column(name="nominativo")
	private String nominativo;
	
	@Column(name="capitale")
	private double capitale;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="nomeBanca")
	private Banca banca;
	
	@OneToMany(cascade = CascadeType.ALL) 
	@JoinColumn(name="assetsInvestitore")
	private List<Asset> assets ;

	public Investitore(String codiceFiscale, String nominativo, double capitale, Banca banca, List<Asset> assets) {
		super();
		this.codiceFiscale = codiceFiscale;
		this.nominativo = nominativo;
		this.capitale = capitale;
		this.banca = banca;
		this.assets = assets;
	}

}