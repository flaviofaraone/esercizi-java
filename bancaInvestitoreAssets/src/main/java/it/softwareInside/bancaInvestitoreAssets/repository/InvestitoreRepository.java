package it.softwareInside.bancaInvestitoreAssets.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.softwareInside.bancaInvestitoreAssets.model.Investitore;

@Repository
public interface InvestitoreRepository extends CrudRepository<Investitore, String> {

}
