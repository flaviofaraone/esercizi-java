package it.softwareInside.bancaInvestitoreAssets.service;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.softwareInside.bancaInvestitoreAssets.model.Asset;
import it.softwareInside.bancaInvestitoreAssets.model.Banca;
import it.softwareInside.bancaInvestitoreAssets.model.Investitore;
import it.softwareInside.bancaInvestitoreAssets.repository.InvestitoreRepository;

@Service
public class InvestitoreService {
	
	@Autowired
	private InvestitoreRepository investitoreRepository;
	
	public void creaInvestitore() {
		
		List<Asset> assets = new ArrayList<>();
		assets.add(new Asset("bitcoin" , 30000.00));
		
		Banca bancaFinta = new Banca("ba" , "coop" , "mondiale");
		
		Investitore investitore = new Investitore("codiceFino" ,"mario rossi" , 1 , bancaFinta , assets);
		
		investitoreRepository.save(investitore);
	}
	
	public void addInvestitore(Investitore investitore) {
		investitoreRepository.save(investitore);
	}
	
	public void rimuoviInvestitore(Investitore investitore) {
		investitoreRepository.delete(investitore);
	}
	
	public List<Investitore> getAllInvestitori(){
		return (List<Investitore>) investitoreRepository.findAll();
	}

}