package it.softwareinside.batteriaTelefono23oneToOne;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BatteriaTelefono23oneToOneApplication {

	public static void main(String[] args) {
		SpringApplication.run(BatteriaTelefono23oneToOneApplication.class, args);
	}

}
