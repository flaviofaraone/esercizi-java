package it.softwareinside.batteriaTelefono23oneToOne.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import it.softwareinside.batteriaTelefono23oneToOne.service.TelefonoService;

@org.springframework.stereotype.Controller
public class Controller {

	@Autowired
	private TelefonoService telefonoService;
	
	@GetMapping(value="/")
	public ModelAndView getHome() {
		ModelAndView model =new ModelAndView();
		model.setViewName("index");
		
		return model;
	}
	
	
//	https://pastebin.com/60BwKLgh
//	https://pastebin.com/Bn4reQwh
//	https://pastebin.com/uCBKbJ3U
//	https://pastebin.com/eeL3R2MX
	
}
