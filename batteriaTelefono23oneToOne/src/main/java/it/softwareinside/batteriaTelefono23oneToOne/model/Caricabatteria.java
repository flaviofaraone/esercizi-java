package it.softwareinside.batteriaTelefono23oneToOne.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@Entity
@Table(name="caricabatteria")
public class Caricabatteria {
	
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name="amperaggio")
	private int amperaggio;
	
	@Column(name="ricaricaVeloce")
	private boolean ricaricaVeloce;

	public Caricabatteria(int amperaggio, boolean ricaricaVeloce) {
		super();
		this.amperaggio = amperaggio;
		this.ricaricaVeloce = ricaricaVeloce;
	}
	
}