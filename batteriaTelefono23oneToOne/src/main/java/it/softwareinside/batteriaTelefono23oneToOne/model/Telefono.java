package it.softwareinside.batteriaTelefono23oneToOne.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name="telefono")
public class Telefono {
	
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	@PrimaryKeyJoinColumn
	@Column(name="id")
	private int id;
	
	@Column(name="marca")
	private String marca;
	
	@Column(name="prezzo")
	private double prezzo;
	
	@Column(name="is_dualSim")
	private boolean isDualSim;

	@OneToOne(targetEntity = Caricabatteria.class ,  cascade = CascadeType.ALL)
	private Caricabatteria caricabatteria;

	public Telefono(String marca, double prezzo, boolean isDualSim, Caricabatteria caricabatteria) {
		super();
		this.marca = marca;
		this.prezzo = prezzo;
		this.isDualSim = isDualSim;
		this.caricabatteria = caricabatteria;
	}

}