package it.softwareinside.batteriaTelefono23oneToOne.repository;

import org.springframework.data.repository.CrudRepository;

import it.softwareinside.batteriaTelefono23oneToOne.model.Caricabatteria;

public interface CaricatoreRepository extends CrudRepository<Caricabatteria, Integer>{

}
