package it.softwareinside.batteriaTelefono23oneToOne.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.softwareinside.batteriaTelefono23oneToOne.model.Telefono;

@Repository
public interface TelefonoRepository extends CrudRepository<Telefono, Integer> {

}
