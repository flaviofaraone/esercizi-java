package it.softwareinside.batteriaTelefono23oneToOne.service;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.softwareinside.batteriaTelefono23oneToOne.model.Caricabatteria;
import it.softwareinside.batteriaTelefono23oneToOne.model.Telefono;
import it.softwareinside.batteriaTelefono23oneToOne.repository.CaricatoreRepository;
import it.softwareinside.batteriaTelefono23oneToOne.repository.TelefonoRepository;

@Service
public class TelefonoService {

	@Autowired
	private TelefonoRepository telefonoRepository;

	private void addTelefono(Telefono telefono) {
		telefonoRepository.save(telefono);
	}

	private void removeTelefono(Telefono telefono) {
		telefonoRepository.delete(telefono);
	}

	private void removeIdTelefono(int id) {
		telefonoRepository.deleteById(id);
	}

	//------------------------------------metodi per riempire tabella telefoni

	public void creaRandomTelefoni() {
		for(int i = 0 ; i < 5 ; i++) {
			telefonoRepository.save(new Telefono(generaMarcaTelefono() 
					, generaPrezzoTelefono()
					, false
					, new Caricabatteria(generaAmpreCaricatore() , generaCVelCaricatore())));
		}
	}

	private String generaMarcaTelefono() {
		String[] vettMarca = {"apple" , "oppo" , "samsung", "nokia"};
		Random random = new Random();

		return vettMarca[random.nextInt(vettMarca.length)];
	}

	private double generaPrezzoTelefono() {
		Random random = new Random();
		return  50 +random.nextInt(1000);
	}

	//----------------random caricatore

	private int generaAmpreCaricatore() {
		int[] vettAmper = {1000 , 2000 , 3000};
		Random random = new Random();

		return vettAmper[random.nextInt(vettAmper.length)];
	}
	
	private boolean generaCVelCaricatore() {
		Random random = new Random();
		
		boolean value = random.nextBoolean();
		
		return value;
	}
}
