package itsoftwareinside.demo30.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@org.springframework.stereotype.Controller
public class Controller {
		
	@GetMapping(value="/")
	public ModelAndView index() {
		ModelAndView model =new ModelAndView();
		model.setViewName("index");
		
		return model;
	}
	
	@GetMapping(value="/age2")
	public ModelAndView age2() {
		ModelAndView model =new ModelAndView();
		model.setViewName("age2");
		
		return model;
	}
	

}
