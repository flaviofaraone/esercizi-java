package it.softwareinside.dogApi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import it.softwareinside.dogApi.model.Dog;

@org.springframework.stereotype.Controller
public class Controller {

	@GetMapping(value="/")
	public ModelAndView getHome() {
		//System.out.println("qui");
		
		RestTemplate restTemplate  = new RestTemplate();
		Dog myDog=restTemplate.getForObject("https://dog.ceo/api/breeds/image/random", Dog.class);
		
		
		ModelAndView model = new ModelAndView();
		model.addObject("dog" , myDog);
		
		model.addObject("insiemeDog" , generaInsiemeDog());
	
		//System.out.println("sono qui");
		//System.out.println(generaInsiemeDog());
		
		model.setViewName("index");
		return model;
	}
	
	private List<Dog> generaInsiemeDog(){
		ArrayList<Dog> myDogs = new ArrayList<>();
		RestTemplate restTemplate  = new RestTemplate();
		
		for(int i = 0 ; i < 4 ; i++) {
			Dog myDog=restTemplate.getForObject("https://dog.ceo/api/breeds/image/random", Dog.class);
			
			myDogs.add(myDog);
		}
		
		return myDogs;
	}
	
	@GetMapping(value="/contatti")
	public ModelAndView getContatti() {
		
		ModelAndView model = new ModelAndView();
		
		System.out.println("qwertyuio");
		
		model.setViewName("contatti");
		return model;	}
	
	
}
