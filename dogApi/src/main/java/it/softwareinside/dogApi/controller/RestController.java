package it.softwareinside.dogApi.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import it.softwareinside.dogApi.model.Dog;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/")
public class RestController {

	
	@GetMapping("/dog")
	public Dog oneDog() {
		Dog dog =new Dog("https:/image" , "www");
		
		return dog;
	}
	
	
	@GetMapping("/numeri")
	public List<Integer> creaNumeri(){
 
	
		ArrayList<Integer> arrayNumeri = new ArrayList<>();
		for(int i = 0 ; i < 30; i++) {
			arrayNumeri.add(    new Random().nextInt(100)   );
		}
		return arrayNumeri;
	}
	
	@GetMapping("/frasi")
	public List<String> fraseDelGiorno(){
		
		ArrayList<String> arrayFrasi = new ArrayList<>();
		
		String frase1 = new String("Dai un pesce a un uomo e lo nutrirai per un giorno."
				+ " Insegnagli a pescare e lo nutrirai per tutta la vita.");
		String frase2 = new String("Se c’è un rimedio, perché te la prendi?"
				+ " E se non c’è un rimedio, perché te la prendi?");
		String frase3 = new String("Quando piove lo stolto impreca contro gli dei,"
				+ " il saggio si procura un ombrello");
		
		arrayFrasi.add(frase1 );
		arrayFrasi.add(frase2 );
		arrayFrasi.add(frase3 );
		
		
		return arrayFrasi;
		
	}
	
	
	@GetMapping("/frase")
	public String frase() {
		
		ArrayList<String> arrayFrasi = new ArrayList<>();
		
		String frase1 = new String("Dai un pesce a un uomo e lo nutrirai per un giorno."
				+ " Insegnagli a pescare e lo nutrirai per tutta la vita.");
		String frase2 = new String("Se c’è un rimedio, perché te la prendi?"
				+ " E se non c’è un rimedio, perché te la prendi?");
		String frase3 = new String("Quando piove lo stolto impreca contro gli dei,"
				+ " il saggio si procura un ombrello");
		
		arrayFrasi.add(frase1 );
		arrayFrasi.add(frase2 );
		arrayFrasi.add(frase3 );
		
		
		Random random = new Random();
		
		return arrayFrasi.get(random.nextInt(3)).toString();
		
		
		
	}
	
	
	
	
	
}
