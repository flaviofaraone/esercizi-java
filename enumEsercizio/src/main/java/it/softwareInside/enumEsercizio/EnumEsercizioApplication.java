package it.softwareInside.enumEsercizio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EnumEsercizioApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnumEsercizioApplication.class, args);
	}

}
