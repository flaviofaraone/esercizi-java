var Persona = /** @class */ (function () {
    function Persona(_name, _surname, _age, _codFiscale) {
        this._name = _name;
        this._surname = _surname;
        this._age = _age;
        this._codFiscale = _codFiscale;
    }
    Object.defineProperty(Persona.prototype, "name", {
        get: function () { return this._name; },
        set: function (_name) {
            this._name = _name;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Persona.prototype, "surname", {
        get: function () { return this._surname; },
        set: function (_surname) {
            this._surname = _surname;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Persona.prototype, "age", {
        get: function () { return this._age; },
        set: function (_age) {
            this._age = _age;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Persona.prototype, "codFiscale", {
        get: function () { return this._codFiscale; },
        set: function (_codFiscale) {
            this._codFiscale = _codFiscale;
        },
        enumerable: false,
        configurable: true
    });
    Persona.prototype.stampaPersona = function () {
        console.log(this._name + " " + this._surname + " ha " + this._age + " anni e il suo codice fiscale è :" + this._codFiscale);
    };
    return Persona;
}());
var firstPersona = new Persona("Flavio", "Faraone", 26, "qwertyuiopò");
firstPersona.stampaPersona();
