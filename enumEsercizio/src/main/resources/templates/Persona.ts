class Persona{
        
    constructor(private _name : string , private _surname : string , private _age: number , private _codFiscale : string){}

    get name(){return this._name}
    get surname(){return this._surname}
    get age(){return this._age}
    get codFiscale(){return this._codFiscale}

    set name(_name){
        this._name=_name;
    }

    set surname(_surname){
        this._surname=_surname;
    }

    set age(_age){
        this._age=_age;
    }

    set codFiscale(_codFiscale){
        this._codFiscale=_codFiscale;
    }
    
    stampaPersona(){
        console.log(this._name +" " + this._surname + " ha "  + this._age + " anni e il suo codice fiscale è : " + this._codFiscale);
    }

}
    
var firstPersona = new Persona("Flavio" , "Faraone" , 26 , "qwertyuiopò");

firstPersona.stampaPersona();