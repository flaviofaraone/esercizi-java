/*   
    Author : esempio di script
*/

console.log("hello world!")

let esercizio = (par1 : string , par2 : string)  =>{

    if(par1.length > par2.length)
        return par1.charAt(0);

    else if (par1.length < par2.length)
        return par2.charAt(0);

    else
        return par1.charAt(0) + par2.charAt(0);
}

console.log(esercizio("ciao" , "gattino"));
    //oppure
    //let risultato = esercizio("ciao" , "gattino");
    //console.log(risultato);

    
//CREA UNA FUNZIONE CHE PRENDA IN INGRESSO 1 VETTORE DI BOOL,
//IL PROGRAMMA RITORNA IL NUMERO DI TRUE PRESENTI NEL VETTORE... 
function esercizio2 (vettBool : boolean[]) {
    
    var cont=0;
        
    for (let i = 0; i < vettBool.length; i++) {
        
        if(vettBool[i] == true)
            cont++;
    }

    return cont;
}

//CREA UNA FUNZIONE CHE PRENDA ingresso una stringa
//ritorna la stringa al contrario.... ... ...

//   ciao ---------------->  oaic 

function inverti ( stringa : string){
    
    var element ="";

    for (let i = stringa.length-1 ; i >= 0; i--) {
        element = stringa[i];
    }

    return element;
}

//Creare una funzione che 
//prenda in ingresso un vettore di numeri
// e un numero
//la funzione ritorna true se il numero e' presente nel vettore
//altrimenti ritorna false...
function numeroPresente (vettNum : number[] , num :number){

    for (let i = 0; i < vettNum.length; i++) {

        if(vettNum[i]== num)
            return true;
    }
}

console.log(numeroPresente([0,1,2,3,1,2,3,1,2,3] , 4));


//creare una funzione che prende in input un vett di string e 
//ritorna un vettore di string dove per ogni elemento del vett in input viene preso solo la prima lettera

function prendiPrimalettera (vettString: string[]){

    var vettPrimaLettera: string[] =[];

    // oppure -----------  vettPrimaLettera= string[];

    for (let i = 0; i < vettString.length; i++) {

        vettPrimaLettera[i]=vettString[i].charAt(0);

    }

    return vettPrimaLettera;
}

    console.log(prendiPrimalettera(["ciao" , "miao"]));


//  ----------------------------------------------------------------------------



let myFirstMessage= "ciao sono una stringa!";

console.log(myFirstMessage.toUpperCase());



///   con una variabile any non ho i metodi si String! quindi farò un casting per poter utilizzare i metodi string !!
let mySecondMessage;
mySecondMessage= "ora sono una stringa!";

// console.log(mySecondMessage.toUppercase);

(<string> mySecondMessage).toUpperCase();
//oppure
(mySecondMessage as string).toUpperCase();



class Point{

  // private _x : number;         ///opzionale
   //private _y : number;

    constructor(private x : number ,private y : number){
    }

    get _x(){ return this.x};
    get _y(){ return this.y};

    set _x(x){
        this.x=x;
    }

    set _y(y){
        this.y=y;
    }

    stampaInfo(){
        console.log("la x vale= " + this.x ,"la y vale= " + this.y);
    }

}
// roberto ha fatto : https://pastebin.com/6GsaeFhv

var myPoint = new Point(10 , 2);  
 
myPoint._x=50;

myPoint.stampaInfo();

console.log("---------------------------------------");