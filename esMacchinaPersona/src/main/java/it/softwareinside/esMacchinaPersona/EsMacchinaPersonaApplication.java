package it.softwareinside.esMacchinaPersona;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsMacchinaPersonaApplication {

	public static void main(String[] args) {
		SpringApplication.run(EsMacchinaPersonaApplication.class, args);
	}

}
