package it.softwareinside.esMacchinaPersona.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import it.softwareinside.esMacchinaPersona.model.Macchina;
import it.softwareinside.esMacchinaPersona.model.Utente;
import it.softwareinside.esMacchinaPersona.service.MacchinaService;
import it.softwareinside.esMacchinaPersona.service.UtenteService;

@org.springframework.stereotype.Controller
public class Controller {
	
	
	@Autowired
	private UtenteService utenteService;
	
	@Autowired
	private MacchinaService macchinaService;
	
	@GetMapping(value="/")
	public ModelAndView getHome() {
		ModelAndView model =new ModelAndView();
		
		model.addObject("utente" , new Utente());
		
		model.addObject("macchina" , new Macchina());
		
		model.setViewName("index");
		return model;
	}

	@PostMapping(value="/postUtente")
	public String PostUtente(@ModelAttribute Utente utente) {
		
		utenteService.addUtente(utente);
		
		return "redirect:/";
	}
	
	@PostMapping(value="/postMacchina")
	public String PostMacchina(@ModelAttribute Macchina macchina) {
		
		macchinaService.addMacchina(macchina);
		
		return "redirect:/";
		
	}

	
		
}
