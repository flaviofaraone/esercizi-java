package it.softwareinside.esMacchinaPersona.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name="macchina")
public class Macchina {
	
	
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	private int id_car;
	
	@Column(name="marca")
	private String marca;
	
	@Column(name="targa")
	private String targa;

	public Macchina(String marca, String targa) {
		super();
		this.marca = marca;
		this.targa = targa;
	}
	
	
}
