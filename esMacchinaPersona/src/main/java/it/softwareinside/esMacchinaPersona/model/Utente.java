package it.softwareinside.esMacchinaPersona.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name="utente")
public class Utente {
	
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	@Column(name="id")
	private int id;
	
	@Column(name="nome")
	private String nome;
	
	@Column(name="cognome")
	private String cognome;
	
	@OneToOne(cascade = CascadeType.ALL)
		@JoinColumn(name="id_car")
		@MapsId
	private Macchina macchina;

	public Utente(String nome, String cognome, Macchina macchina) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.macchina = macchina;
	} 
	

}
