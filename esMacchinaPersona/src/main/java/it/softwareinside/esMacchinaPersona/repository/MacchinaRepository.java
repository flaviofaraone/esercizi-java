package it.softwareinside.esMacchinaPersona.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.softwareinside.esMacchinaPersona.model.Macchina;
@Repository
public interface MacchinaRepository extends CrudRepository<Macchina, Integer> {

}
