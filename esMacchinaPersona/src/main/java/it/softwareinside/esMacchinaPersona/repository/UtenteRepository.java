package it.softwareinside.esMacchinaPersona.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.softwareinside.esMacchinaPersona.model.Utente;
@Repository
public interface UtenteRepository extends CrudRepository<Utente, Integer> {
	
	
	

}
