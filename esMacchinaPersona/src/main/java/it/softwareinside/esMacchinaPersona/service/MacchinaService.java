package it.softwareinside.esMacchinaPersona.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.softwareinside.esMacchinaPersona.model.Macchina;
import it.softwareinside.esMacchinaPersona.repository.MacchinaRepository;

@Service
public class MacchinaService {
	
	
	@Autowired
	private MacchinaRepository macchinaRepository;
	
	
	public void addMacchina(Macchina macchina) {
		macchinaRepository.save(macchina);
	}
	
	public void removeMacchina(Macchina macchina) {
		macchinaRepository.delete(macchina);
	}
	
	public void removeIdMacchina(int id) {
		macchinaRepository.deleteById(id);
	}
	

}