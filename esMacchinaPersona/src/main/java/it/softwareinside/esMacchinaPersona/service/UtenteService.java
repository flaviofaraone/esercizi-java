package it.softwareinside.esMacchinaPersona.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.softwareinside.esMacchinaPersona.model.Utente;
import it.softwareinside.esMacchinaPersona.repository.UtenteRepository;

@Service
public class UtenteService {
	
	
	@Autowired
	private UtenteRepository utenteRepository;
	
	
	public void addUtente(Utente utente) {
		utenteRepository.save(utente);
	}
	
	public void removeUtente(Utente utente) {
		utenteRepository.delete(utente);
	}
	
	public void removeIdUtente(int id) {
		utenteRepository.deleteById(id);
	}
	

}