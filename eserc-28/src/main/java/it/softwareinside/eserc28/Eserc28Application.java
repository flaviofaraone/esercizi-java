package it.softwareinside.eserc28;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Eserc28Application {

	public static void main(String[] args) {
		SpringApplication.run(Eserc28Application.class, args);
	}

}
