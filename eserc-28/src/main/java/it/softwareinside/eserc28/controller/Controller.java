package it.softwareinside.eserc28.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import it.softwareinside.eserc28.model.Persona;
import it.softwareinside.eserc28.repository.PersonaRepository;

@org.springframework.stereotype.Controller
public class Controller {
	
	@Autowired
	private PersonaRepository personaRepository;
	
	@GetMapping(value="/")
	public ModelAndView index() {
		ModelAndView model = new ModelAndView();
		 model.setViewName("index");
		 
		 model.addObject("firstPersona" , personaRepository.findAll());
		 
		 return model;
	}
	
	@GetMapping(value="inserisci")
	public String inserisci() {
		
		personaRepository.save(new Persona("giuseppe" , 3));
		
		return "redirect:/";
	}

}