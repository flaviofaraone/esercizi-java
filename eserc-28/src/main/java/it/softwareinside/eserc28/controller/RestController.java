package it.softwareinside.eserc28.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import it.softwareinside.eserc28.model.Persona;
import it.softwareinside.eserc28.repository.PersonaRepository;
import it.softwareinside.eserc28.service.PersonaService;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api")
public class RestController {
	
	@Autowired
	private PersonaService personaService; 
	
	@GetMapping("/persona")
	public ArrayList<Persona> tuttelepersone(){
		return (ArrayList<Persona>)personaService.getAll();
	}
	
	
}
