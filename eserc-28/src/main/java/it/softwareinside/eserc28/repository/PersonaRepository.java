package it.softwareinside.eserc28.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.softwareinside.eserc28.model.Persona;
@Repository
public interface PersonaRepository extends CrudRepository<Persona, Integer>{

}
