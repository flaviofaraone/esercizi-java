package it.softwareinside.eserc28.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.softwareinside.eserc28.model.Persona;
import it.softwareinside.eserc28.repository.PersonaRepository;

@Service
public class PersonaService {
	
	@Autowired
	private PersonaRepository personaRepository;
	
	public ArrayList<Persona> getAll(){
		return (ArrayList<Persona>)personaRepository.findAll();
	}
	
	public void addPersona(Persona persona) {
		personaRepository.save(persona);
	}
	
	public void deletePersona(Persona persona) {
		personaRepository.delete(persona);
	}
	
	public void deleteByIdPersona(int id) {
		personaRepository.deleteById(id);
	}

}
