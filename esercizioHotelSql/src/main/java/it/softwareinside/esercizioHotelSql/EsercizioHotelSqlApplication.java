package it.softwareinside.esercizioHotelSql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsercizioHotelSqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(EsercizioHotelSqlApplication.class, args);
	}

}
