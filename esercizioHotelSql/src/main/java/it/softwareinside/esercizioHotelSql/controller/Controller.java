package it.softwareinside.esercizioHotelSql.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import it.softwareinside.esercizioHotelSql.model.Camera;
import it.softwareinside.esercizioHotelSql.service.CameraService;

@org.springframework.stereotype.Controller
public class Controller {
	
	@Autowired
	private CameraService cameraService;
	
	@GetMapping(value="/")
	public ModelAndView getHome() {
		ModelAndView model =new ModelAndView();
		model.setViewName("index");
		
		model.addObject("camera" , new Camera());
		
		model.addObject("listaCamereDisponibili" , cameraService.getAllCamereAvalaible());
		
		return model;

	}
	
	@PostMapping(value="crea-camera")
	public String aggiungiCamera(@ModelAttribute Camera camera) {
		
		cameraService.aggiungiCamera(camera);
		
		return "redirect:/";
	}
	
	@GetMapping(value="cancella-camera")
	public String rimuoviCamera(@RequestParam(name="id") Integer id) {
		
		cameraService.rimuoviCamera(id);
		
		return "redirect:/";
	}
	

}
