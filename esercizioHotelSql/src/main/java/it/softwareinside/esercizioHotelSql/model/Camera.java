package it.softwareinside.esercizioHotelSql.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor

@Entity
@Table(name="camera")
public class Camera {

	
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	@Column(name="id")
	private int id;
	
	@Column(name="nomeStanza")
	private String nomeStanza;
	
	@Column(name="prezzoStanza")
	private int prezzoStanza;
	
	@Column(name="isAvailable")
	private boolean isAvailable;
	
	@Column(name="tipologia")
	private String tipologia;
	
	
	
}
