package it.softwareinside.esercizioHotelSql.repository;
import java.util.ArrayList;
import java.util.List;

/**
 * findByNomeStanza(); // ritorna i dati di una stanza
			  		// trovata per nomi
			 
	findByPrezzo(double prezzo)  // ritorna tutte le camere con quel prezzo
	findByPrezzo(min , max)
	findTutteCamereDisponibili //ritorna tutte le camere disponibili.
 */
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.softwareinside.esercizioHotelSql.model.Camera;
@Repository
public interface CameraRepository extends CrudRepository<Camera , Integer> {
	

	@Query("select c from Camera c where c.nomeStanza like ?1")
	Camera findByNomeStanza(String nomeStanza);
	
//	@Query("select c from Camera c where c.prezzoStanza == ?1 ")
//	List<Camera>findByPrezzo (int prezzo);
//	
//	@Query("select c from Camera c where c.prezzoStanza == ?1 ")
//	ArrayList<Camera> findByPrezzo (double prezzo);
//	
//	@Query("select c from Camera c where c.prezzoStanza >=?1 and c.prezzoStanza <=?2")
//	ArrayList<Camera> findBetweenPrezzo (double min , double max);
//	
	
	@Query(" select c from Camera c where c.isAvailable = true ")
	ArrayList<Camera> findCamereDisponibili();
	
	
	
}
