package it.softwareinside.esercizioHotelSql.service;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.softwareinside.esercizioHotelSql.model.Camera;
import it.softwareinside.esercizioHotelSql.repository.CameraRepository;

@Service
public class CameraService {
	
	@Autowired
	private CameraRepository cameraRepository;

	public void aggiungiCamera(Camera camera) {
		cameraRepository.save(camera);
	}
	
	public void rimuoviCamera(int id ) {
		cameraRepository.deleteById(id);
	}
	
	
	public List<Camera> getAllCamereAvalaible(){
		return cameraRepository.findCamereDisponibili();
	}
	
//	public void inviaEmail() {
//		if(cameraRepository.findCamereDisponibili().size() < 5)
//			System.out.println("mail al gestore");
//	}
	
	
	
	
}
