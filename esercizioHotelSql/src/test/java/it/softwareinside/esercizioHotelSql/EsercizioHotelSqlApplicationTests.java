package it.softwareinside.esercizioHotelSql;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import it.softwareinside.esercizioHotelSql.model.Camera;
import it.softwareinside.esercizioHotelSql.repository.CameraRepository;

@SpringBootTest
class EsercizioHotelSqlApplicationTests {
	
	@Autowired
	private CameraRepository cameraRepository;
	
	@Test
	void contextLoads() {
		
		Camera camera =cameraRepository.findByNomeStanza("imperiale1");
		
		assertEquals(camera.getNomeStanza(), "imperiale1");
		assertEquals(camera.getPrezzoStanza() , 300);
		assertEquals(camera.getId() , 1);

//		ArrayList<Camera> camere = (ArrayList<Camera>)cameraRepository.findByPrezzo(300);
//		
//		assertEquals(camere.size(), 1);
//		
	}

}
