package it.softwareinside.esercizioN29pomeriggio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsercizioN29pomeriggioApplication {

	public static void main(String[] args) {
		SpringApplication.run(EsercizioN29pomeriggioApplication.class, args);
	}

}
