package it.softwareinside.esercizioN29pomeriggio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import it.softwareinside.esercizioN29pomeriggio.model.Prodotto;
import it.softwareinside.esercizioN29pomeriggio.service.ServiceProdotto;

@org.springframework.stereotype.Controller
public class Controller {
	
	@Autowired
	private ServiceProdotto serviceProdotto;
	
	
	@GetMapping(value="/")
	public ModelAndView index() {
		
		ModelAndView model = new ModelAndView();
		model.setViewName("index");
		
		
		return model;
	}
	
	@GetMapping(value="/creaProdotti")
	public String crea () {
		
		serviceProdotto.riempiProdotto(15);
		
		return "redirect:/";
	}
	
	@GetMapping(value="/prodotti")
	public ModelAndView prodotti() {
		
		ModelAndView model = new ModelAndView();
		model.setViewName("prodotti");
						
		model.addObject("prodotto" , new Prodotto());
		
		model.addObject("listaProdotti", serviceProdotto.getAllProdotti());
		
		return model;		
	}
	
	@PostMapping(value="modificaProdotto")
	public String modificaProdotto(@ModelAttribute()Prodotto prodotto) {
		serviceProdotto.modificaProdotto(prodotto);
		
		return "redirect:/prodotti";
	}
	
	
	@GetMapping(value="/cancellaProdotto")
	public String cancella(@RequestParam(name="id") int id) {
		serviceProdotto.deleteById(id);
		return "redirect:/prodotti";
	}
	
	

}
