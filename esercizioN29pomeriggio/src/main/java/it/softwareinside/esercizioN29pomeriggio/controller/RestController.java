package it.softwareinside.esercizioN29pomeriggio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import it.softwareinside.esercizioN29pomeriggio.model.Prodotto;
import it.softwareinside.esercizioN29pomeriggio.service.ServiceProdotto;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api")
public class RestController {
	
	@Autowired
	private ServiceProdotto serviceProdotto;
	
	@GetMapping("/getProdottoById")
	public Prodotto getProdottoById(@RequestParam(name="id")int id) {
		
		return serviceProdotto.getProdottoById(id);
	}
	
}
