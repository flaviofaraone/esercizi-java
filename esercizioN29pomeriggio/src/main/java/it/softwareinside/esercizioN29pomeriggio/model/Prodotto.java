package it.softwareinside.esercizioN29pomeriggio.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Entity
@Data
@Table(name="prodotto")
public class Prodotto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private int id;
	
	@Column(name="nome")
	private String nome;

	@Column(name="quantita")
	private int quantita;

	public Prodotto(String nome, int quantita) {
		super();
		this.nome = nome;
		this.quantita = quantita;
	}
	
}