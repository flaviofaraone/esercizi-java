package it.softwareinside.esercizioN29pomeriggio.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.softwareinside.esercizioN29pomeriggio.model.Prodotto;

@Repository
public interface ProdottoRepository extends CrudRepository<Prodotto, Integer> {

}
