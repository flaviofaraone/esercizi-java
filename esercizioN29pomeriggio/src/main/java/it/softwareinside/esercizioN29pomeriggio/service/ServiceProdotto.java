package it.softwareinside.esercizioN29pomeriggio.service;
import java.util.List;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import it.softwareinside.esercizioN29pomeriggio.model.Prodotto;
import it.softwareinside.esercizioN29pomeriggio.repository.ProdottoRepository;

@Service
public class ServiceProdotto {
	
	@Autowired
	private ProdottoRepository prodottoRepository;
	
	public void AddProdotto(Prodotto prodotto) {
		prodottoRepository.save(prodotto);
	}
	
	public List<Prodotto> getAllProdotti(){
		return (List<Prodotto>) prodottoRepository.findAll();
	}

	public Prodotto getProdottoById(int id) {
		return prodottoRepository.findById(id).get();
	}
	
	public void deleteById(int id) {
		prodottoRepository.deleteById(id);
	}
	
	public void deleteByProdotto(Prodotto prodotto) {
		prodottoRepository.delete(prodotto);
	}
	
	public void modificaProdotto(Prodotto prodotto) {
		prodottoRepository.save(prodotto);
	}

	public void riempiProdotto(int numeroProdotti) {
		
		for(int i = 0 ; i < numeroProdotti ; i++) {
		
			String[] names = {"cellulare" , "forno", "computer", "monitor" ,"occhiali","casse", "tastiera"};
			String nome = names[new Random().nextInt(names.length)];
			int quantita = new Random().nextInt(99) +1 ;	
			Prodotto prodotto = new Prodotto(nome, quantita);
			prodottoRepository.save(prodotto);
		
		}
		
	}
	
}