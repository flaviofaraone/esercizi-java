package it.softwareinside.esercizioSpring18;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsercizioSpring18Application {

	public static void main(String[] args) {
		SpringApplication.run(EsercizioSpring18Application.class, args);
	}

}
