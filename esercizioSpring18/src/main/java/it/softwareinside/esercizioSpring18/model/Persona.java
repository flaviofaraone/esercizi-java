package it.softwareinside.esercizioSpring18.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Persona {

	private String nome , cognome;
	private int eta;
	
}
