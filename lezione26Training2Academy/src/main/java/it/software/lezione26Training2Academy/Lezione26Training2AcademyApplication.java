package it.software.lezione26Training2Academy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lezione26Training2AcademyApplication {

	public static void main(String[] args) {
		SpringApplication.run(Lezione26Training2AcademyApplication.class, args);
	}

}
