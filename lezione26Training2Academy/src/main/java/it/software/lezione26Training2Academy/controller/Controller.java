package it.software.lezione26Training2Academy.controller;

import org.springframework.web.servlet.ModelAndView;

import it.software.lezione26Training2Academy.model.Weather;
import it.software.lezione26Training2Academy.service.WeatherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@org.springframework.stereotype.Controller
public class Controller {

	
	@Autowired
	private WeatherService weatherService;
	
	
	@GetMapping(value="/")
	public ModelAndView model() {
		ModelAndView model = new ModelAndView();
		model.setViewName("index");
		
		/*
		RestTemplate restTemplate = new RestTemplate();
		
		Results results = 
		restTemplate.getForObject("https://randomuser.me/api/", Results.class);
		
		System.out.println(   results.getResults().get(0)  );
		
		//String nome = results.getResults().get(0).getName().getFirst();
		results.getResults().get(0).getGender();
		
		
		model.addObject("user", results );
		*/
		//Weather weather = weatherService.getWeather("milan");
		//System.out.println(weather);
		
		return model;
	}
	
	
	@GetMapping(value="/meteo")
	public ModelAndView meteo(@RequestParam(name="city") String city ) {
		
		ModelAndView model = new ModelAndView();
		model.setViewName("meteo");
		
		Weather weather = weatherService.getWeather(city);
		
		model.addObject("meteo", weather);
		

		return model;
	}
	
	
	
	
	
}
