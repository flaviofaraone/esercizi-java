package it.software.lezione26Training2Academy.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Name {
	
	private String title;
	private String first;
	private String last;
	
}
