package it.software.lezione26Training2Academy.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Persona {

	private String gender;
	private Name name;
	
}
