package it.software.lezione26Training2Academy.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Results {

	private ArrayList<Persona> results;
	
	
}
