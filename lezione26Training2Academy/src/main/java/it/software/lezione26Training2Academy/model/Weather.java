package it.software.lezione26Training2Academy.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

/**
 * 
 * 
 * https://goweather.herokuapp.com/weather/city
 * @author mr
 *
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Weather {

	private String temperature;
	private String wind;
	private String description;
	private List<Forecast> forecast;
}


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
class Forecast {
	private String day;
	private String temperature;
	private String wind;
}



