package it.software.lezione26Training2Academy.service;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import it.software.lezione26Training2Academy.model.Weather;

@Service
public class WeatherService {
	
	//costante
	private final String URL = "https://goweather.herokuapp.com/weather/";
	
	
	/**
	 * 
	 * @param city
	 * @return un object di tipo Weather
	 */
	public Weather getWeather(String city) {
		
		RestTemplate restTemplate = new RestTemplate();
		Weather weather = restTemplate.getForObject(URL + city, Weather.class);
		
		return weather;
	}
	
	
	
}
