package it.softwareInside.lezione29AcademySpringBoot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lezione29AcademySpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(Lezione29AcademySpringBootApplication.class, args);
	}

}
