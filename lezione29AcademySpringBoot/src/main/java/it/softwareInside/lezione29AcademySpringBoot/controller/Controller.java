package it.softwareInside.lezione29AcademySpringBoot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import it.softwareInside.lezione29AcademySpringBoot.model.Persona;
import it.softwareInside.lezione29AcademySpringBoot.service.ServicePersona;

@org.springframework.stereotype.Controller
public class Controller {

	
	@Autowired
	private ServicePersona personaService;
	
	
	@GetMapping(value="/")
	public ModelAndView index() {
		
		ModelAndView model = new ModelAndView();
		model.setViewName("index");
		
		
		return model;
	}
	
	
	@GetMapping(value="/persone")
	public ModelAndView persone() {
		
		ModelAndView model = new ModelAndView();
		model.setViewName("persone");
		
		
		Persona p2 = new Persona("", 0);
		
		model.addObject("persona" , p2); // mi serve per la form...
		
		model.addObject("listaPersone", personaService.getAllPersone());
		
		
		return model;
	} 
	
	
	@GetMapping(value="/cancellaPersona")
	public String cancella(@RequestParam(name="id") int id) {
		
		personaService.deleteById(id);
		return "redirect:/persone";

	}
	
	
	@GetMapping(value="/crea")
	public String crea() {
		
		personaService.riempiPersona(10);
		
		return "redirect:/";
	}
	
	
	
	@PostMapping(value="modificaPersona")
	public String modificaPersona(@ModelAttribute()Persona persona) {
		
		personaService.modificaPersona(persona);
		
		return "redirect:/persone";
	}
	
	
}
