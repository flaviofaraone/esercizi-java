package it.softwareInside.lezione29AcademySpringBoot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import it.softwareInside.lezione29AcademySpringBoot.model.Persona;
import it.softwareInside.lezione29AcademySpringBoot.service.ServicePersona;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api")
public class RestController {

	@Autowired
	private ServicePersona personaService;
	
	
	@GetMapping("/getPersonaById")
	public Persona getPersonaById(@RequestParam(name="id")int id) {
		
		
		return personaService.getPersonaById(id);
	}
	
	
}
