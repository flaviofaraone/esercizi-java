package it.softwareInside.lezione29AcademySpringBoot.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.softwareInside.lezione29AcademySpringBoot.model.Persona;

@Repository
public interface PersonaRepository extends CrudRepository<Persona, Integer>{

}
