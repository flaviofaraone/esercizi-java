package it.softwareInside.lezione29AcademySpringBoot.service;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.softwareInside.lezione29AcademySpringBoot.model.Persona;
import it.softwareInside.lezione29AcademySpringBoot.repository.PersonaRepository;

@Service
public class ServicePersona {

	@Autowired
	private PersonaRepository personaRepository;
	
	
	public Persona getPersonaById(int id) {
		return personaRepository.findById(id).get();
	}
	
	public void deleteById(int id) {
		personaRepository.deleteById(id);
	}
	
	public void deleteByPersona(Persona persona) {
		personaRepository.delete(persona);
	}
	
	
	public void aggiungiPersona(Persona persona) {
		personaRepository.save(persona);
	}
	
	public List<Persona> getAllPersone(){
		return (List<Persona>) personaRepository.findAll();
	}
	
	
	public void modificaPersona(Persona persona) {
		personaRepository.save(persona);
	}
	
	
	public void riempiPersona(int numeroPersone) {
		
		for(int i = 0 ; i < numeroPersone ; i++) {
		
			String[] names = {"mario" , "luigi", "paola", 
							"michela" , "isa" ,"pluto","beatrice", "michelone"};
			String nome = names[new Random().nextInt(names.length)];
			int eta = new Random().nextInt(90) +1 ;	
			Persona persona = new Persona(nome, eta);
			personaRepository.save(persona);
		
		}
		
	}
	
	
}
