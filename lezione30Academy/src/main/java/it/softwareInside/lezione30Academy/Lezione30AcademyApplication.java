package it.softwareInside.lezione30Academy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lezione30AcademyApplication {

	public static void main(String[] args) {
		SpringApplication.run(Lezione30AcademyApplication.class, args);
	}

}
