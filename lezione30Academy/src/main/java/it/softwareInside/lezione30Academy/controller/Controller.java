package it.softwareInside.lezione30Academy.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import it.softwareInside.lezione30Academy.model.Persona;
import it.softwareInside.lezione30Academy.model.Treno;
import it.softwareInside.lezione30Academy.service.TrenoService;

@org.springframework.stereotype.Controller
public class Controller {

	@Autowired
	private TrenoService trenoService;
	
	
	@GetMapping(value="/")
	public ModelAndView index() {
		
		ModelAndView model = new ModelAndView();
		model.setViewName("index");
		
		return model;
	}
	
	
	@GetMapping(value="/age2")
	public ModelAndView age2() {
		
		ModelAndView model = new ModelAndView();
		model.setViewName("age-2");
		
		return model;
	}
	
	
	@GetMapping(value="/treno")
	public String treno() {
		
		trenoService.riempiTreno();
		
		return "redirect:/";
	}
	
	

	@GetMapping(value="/treno-index")
	public ModelAndView trenoIndex(){
		ModelAndView model = new ModelAndView();
		
		model.addObject("treno", new Treno());
		
		model.setViewName("treno-index");
		
		return model;
	}
	
	@PostMapping(value="/postaTreno")
	public String postaTreno( @ModelAttribute Treno treno,@RequestParam(name="persone") String[] nomiDellePersone){
		
		
		ArrayList<Persona> personeNelTreno = new ArrayList<>();
		
		for(String name : nomiDellePersone)
			personeNelTreno.add(  new Persona(name) );
		
		
		treno.setPersone(personeNelTreno);
		
		trenoService.saveTreno(treno);
		
		return "redirect:/treno-index";
	}
		
}