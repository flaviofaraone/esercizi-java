package it.softwareInside.lezione30Academy.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@Data
@Entity
@Table(name="treno")
public class Treno {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private int id;
	
	
	@Column(name="nome_treno")
	private String nomeTreno;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="trenoId")
	private List<Persona> persone;

	public Treno(
			String nomeTreno, 
			List<Persona> persone) {
		super();
		this.nomeTreno = nomeTreno;
		this.persone = persone;
	}
	
}