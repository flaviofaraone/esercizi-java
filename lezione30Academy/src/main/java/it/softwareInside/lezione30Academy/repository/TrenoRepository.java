package it.softwareInside.lezione30Academy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.softwareInside.lezione30Academy.model.Treno;

@Repository
public interface TrenoRepository extends JpaRepository<Treno, Integer>{

}
