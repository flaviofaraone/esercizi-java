package it.softwareInside.lezione30Academy.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.softwareInside.lezione30Academy.model.Persona;
import it.softwareInside.lezione30Academy.model.Treno;
import it.softwareInside.lezione30Academy.repository.TrenoRepository;

@Service
public class TrenoService {

	@Autowired
	private TrenoRepository trenoRepository;
	
	
	public void riempiTreno() {
		
		ArrayList<Persona> persone = new ArrayList<>();
		persone.add(new Persona("persona generica"));

		Treno treno = new Treno("FRECCIA bianca", persone);
		
		trenoRepository.save(treno);

	}
	
	
	public void saveTreno(Treno treno) {
		
		trenoRepository.save(treno);
	}
	
}
