package itsoftwareinside.manyToMany.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import itsoftwareinside.manyToMany.service.ProfessoreService;

@org.springframework.stereotype.Controller
public class Controller {
	
	@Autowired
	private ProfessoreService professoreService;	
	@GetMapping(value="/")
	public ModelAndView index() {
		ModelAndView model =new ModelAndView();
		model.setViewName("index");
		
		model.addObject("professore" , professoreService.getProfessore(1));
				
		return model;
	}

}