package itsoftwareinside.manyToMany.model;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table
public class Professore {
	
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO )
	@Column
	private int id_professore;
	
	@Column
	private String materia;
	
	@Column
	private String nome;
	
	@OneToMany(cascade = CascadeType.ALL) 
	@JoinColumn(name="idStudenteProfessore")
	private List<Studente> studenti;

	public Professore(String materia, String nome, List<Studente> studenti) {
		super();
		this.materia = materia;
		this.nome = nome;
		this.studenti = studenti;
	}

}