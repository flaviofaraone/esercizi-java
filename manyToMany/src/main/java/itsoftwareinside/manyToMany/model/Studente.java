package itsoftwareinside.manyToMany.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table
public class Studente {
	
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO )
	@Column()
	private int idStudente;
	
	@Column
	private int eta;
	
	@Column
	private String nome;

	public Studente(int eta, String nome) {
		super();
		this.eta = eta;
		this.nome = nome;
	}

//	@Column
//	private Professore professore;

}