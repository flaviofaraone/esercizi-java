package itsoftwareinside.manyToMany.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import itsoftwareinside.manyToMany.model.Professore;
@Repository
public interface ProfessoreRepository extends CrudRepository<Professore, Integer> {
	
		
}
