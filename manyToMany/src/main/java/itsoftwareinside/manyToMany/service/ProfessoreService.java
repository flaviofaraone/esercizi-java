package itsoftwareinside.manyToMany.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import itsoftwareinside.manyToMany.model.Professore;
import itsoftwareinside.manyToMany.repository.ProfessoreRepository;

@Service
public class ProfessoreService {
		
	@Autowired
	private ProfessoreRepository professoreRepository;

	public void addProfessore(Professore professore) {
		professoreRepository.save(professore);
	}
	
	public void deleteProfessore(Professore professore) {
		professoreRepository.delete(professore);
	}
	
	public void deleteByIdProfessore(int id) {
		professoreRepository.deleteById(id);
	}
	
	public Professore getProfessore(int id) {
		return professoreRepository.findById(id).get();
	}
}