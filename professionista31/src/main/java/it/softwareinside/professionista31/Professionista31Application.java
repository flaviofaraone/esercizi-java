package it.softwareinside.professionista31;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Professionista31Application {

	public static void main(String[] args) {
		SpringApplication.run(Professionista31Application.class, args);
	}

}