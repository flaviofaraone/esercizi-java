package it.softwareinside.professionista31.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import it.softwareinside.professionista31.service.ProfessionistaService;

@org.springframework.stereotype.Controller
public class Controller {
	
	@Autowired
	private ProfessionistaService professionistaService;
	
	@GetMapping(value="/")
	public ModelAndView index() {
		ModelAndView model =new ModelAndView();
		model.setViewName("index");
				
		model.addObject( "professionisti" , professionistaService.findProfessionisti());
		
		return model;
	}
	
	@GetMapping(value="/creaProfessionista")
	public String professionista() {
		
		professionistaService.creaProfessionista();
		
		return "redirect:/";
	}

}