package it.softwareinside.professionista31.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name="cliente")
public class Cliente {
	
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO )
	@Column(name="idCliente")
	private int idCliente;
	
	@Column(name="nome")
	private String nome;
	
	@Column(name="settore")
	private String settore;
	
	
	public Cliente(String nome, String settore) {
		super();
		this.nome = nome;
		this.settore = settore;
	}
}