package it.softwareinside.professionista31.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name="partitaIva")
public class PartitaIva {

	@Id
	@Column(name="idIva" ,length =5)
	private String idIva; 
	
	@Column(name="tipologia")
	private String tipologia;

	public PartitaIva(String idIva, String tipologia) {
		super();
		this.idIva = idIva;
		this.tipologia = tipologia;
	}
}