package it.softwareinside.professionista31.model;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name="professionista")
public class Professionista {
		
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO )
	@Column(name="idProfessionista")
	private int idProfessionista;
	
	@Column(name="nome")
	private String nome;
	
	@Column(name="cognome")
	private String cognome;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="idIvaProfessionista")
	private PartitaIva partitaIva;
	
	@OneToMany(cascade = CascadeType.ALL) 
	@JoinColumn(name="idProfclliente")
	private List<Cliente> clienti;

	public Professionista( String nome, String cognome, PartitaIva partitaIva, List<Cliente> clienti) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.partitaIva = partitaIva;
		this.clienti = clienti;
	}

}