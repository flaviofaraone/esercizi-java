package it.softwareinside.professionista31.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.softwareinside.professionista31.model.Professionista;
@Repository
public interface ProfessionistaRepository extends CrudRepository<Professionista, Integer> {

}
