package it.softwareinside.professionista31.service;
import java.util.ArrayList;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import it.softwareinside.professionista31.model.Cliente;
import it.softwareinside.professionista31.model.PartitaIva;
import it.softwareinside.professionista31.model.Professionista;

import it.softwareinside.professionista31.repository.ProfessionistaRepository;
@Service
public class ProfessionistaService {

	@Autowired
	private ProfessionistaRepository professionistaRepository;
	
	public ArrayList<Professionista> findProfessionisti() {
		return (ArrayList<Professionista>) professionistaRepository.findAll();
	}
	
	public void creaProfessionista() {
		
		ArrayList<Cliente> clienti= new ArrayList<>();
		clienti.add(new Cliente("mario" , "consulenza"));
		
		PartitaIva ivaFinta = new PartitaIva(randomPIva(), "scarparo");
		
		Professionista professionista= new Professionista( "nomeDefualt" , "cognDefault" , ivaFinta , clienti);
		
		professionistaRepository.save(professionista);
	}
	
	public String randomPIva() {
		String [] vettoreStringhe = {"a" , "b" , "c" , "d" , "e" , "f" , "g" , "h" , "i"};
		
		String pIva ="";
		
		Random rand = new Random();
		for(int i=0; i<5 ; i++) {
			pIva += vettoreStringhe[rand.nextInt(vettoreStringhe.length)];
		}
		
		return pIva;
	}
	
	
	public void addProfessionista(Professionista professionista) {
		professionistaRepository.save(professionista);
	}
	
	public void rimuoviProfessionista(Professionista professionista) {
		professionistaRepository.delete(professionista);
	}
	
}