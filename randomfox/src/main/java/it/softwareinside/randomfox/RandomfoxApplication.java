package it.softwareinside.randomfox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RandomfoxApplication {

	public static void main(String[] args) {
		SpringApplication.run(RandomfoxApplication.class, args);
	}

}
