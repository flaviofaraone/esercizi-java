package it.softwareinside.randomfox.controller;


import java.util.ArrayList;
import java.util.List;
 
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
 
import it.softwareinside.randomfox.model.Fox;
 
@org.springframework.stereotype.Controller
public class Controller {
 
	@GetMapping(value="/")
	public ModelAndView getHome() {
 
		RestTemplate restTemplate  = new RestTemplate();
		Fox myFox =restTemplate.getForObject("https://randomfox.ca/floof/?ref=apilist.fun", 
									Fox.class);
 
		ModelAndView model = new ModelAndView();
		model.addObject("fox", myFox);
 
		model.addObject("insiemeFox", generaInsiemeFox());
 
 
		model.setViewName("index");
		return model;
	}
 
 
	private List<Fox> generaInsiemeFox(){
		ArrayList<Fox> myFoxs = new ArrayList<>();
		RestTemplate restTemplate  = new RestTemplate();
 
		for(int i = 0 ; i < 4 ; i++) {
			Fox myFox =restTemplate.getForObject("https://randomfox.ca/floof/?ref=apilist.fun", 
					Fox.class);
			myFoxs.add(myFox);
		}
 
		return myFoxs;
	}
 
 
 
 
}
