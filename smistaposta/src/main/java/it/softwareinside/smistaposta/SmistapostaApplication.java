package it.softwareinside.smistaposta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmistapostaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmistapostaApplication.class, args);
	}

}
