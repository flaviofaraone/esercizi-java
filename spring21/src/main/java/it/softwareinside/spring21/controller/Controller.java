package it.softwareinside.spring21.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import it.softwareinside.spring21.model.Automobile;
import it.softwareinside.spring21.repository.AutomobileRepository;

import java.awt.List;

import org.springframework.beans.factory.annotation.Autowired;


@org.springframework.stereotype.Controller
public class Controller {
	
	@Autowired
	private AutomobileRepository automobileRepository;
	
	@GetMapping(value="/")
	public ModelAndView getHome() {
		ModelAndView model =new ModelAndView();
		model.setViewName("index");

		model.addObject("automobile" , new Automobile()) ;
		
		model.addObject( "lista_auto" , automobileRepository.findAll());
		
		return model;

	}
	
//	@GetMapping("/findDiesel")
//	public ModelAndView diesel() {
//		ModelAndView model = new ModelAndView();
//		model.setViewName("diesel");
//		
////		List<Automobile> diesel = AutomobileRepository.findByFuel("diesel");
//		model.addObject("diesel", diesel);
//		return model;
//	}
//	 
	
	
	@PostMapping(value="/postAutomobile")
	public String PostAutomobile(@ModelAttribute Automobile automobile) {
		
		
		if (automobile != null)
			automobileRepository.save(automobile);
		
		return "redirect:/";
	}

}
