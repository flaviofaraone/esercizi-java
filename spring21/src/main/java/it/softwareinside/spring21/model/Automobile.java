package it.softwareinside.spring21.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor

@Entity
@Table(name="automobile")
public class Automobile {

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	@Column(name="id")
	private int id;
	
	@Column(name="marca")
	private String marca;
	
	@Column(name="alimentazione")
	private String alimentazione;
	
	@Column(name="cilindrata")
	private int cilindrata;
	
	@Column(name="data_acquisto")
	private Date dataAcquisto;
	
	@Column(name="colore")
	private String colore;
	
	@Column(name="prezzo")
	private double prezzo;
}
