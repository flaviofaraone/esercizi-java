package it.softwareinside.spring21.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.softwareinside.spring21.model.Automobile;
@Repository
public interface AutomobileRepository extends CrudRepository<Automobile, Integer> {

	
	@Query("select c from Automobile c where c.alimentazione like %?1")
	  List<Automobile> findByFuel(String alimentazione);
	
	
	/**
	 * creare una query  che prenda in input la cilindrata e
	 * ritorni tutti i veicoli che hanno cilindrata uguale o maggiore
	 */
	
	
	@Query(" select c from Automobile c where c.cilindrata >=1")
	 List<Automobile> findByCc(int Cilindrata);
	
	
	
	/**
	 * creare una query  ritorni una lista di auto dove ho 
	 * 2 parametri di ingresso min e max
	 */
	
	@Query(" select c from Automobile c where c.cilindrata >=?1 and c.cilindrata <=?2")
	 List<Automobile> findBetweenCc(int min, int max);
	
	/**
	 *somma valori auto
	 */
	
	@Query("select sum(prezzo) from Automobile")
	double totalePrezzo();
	
	
}
 