package it.softwareInside.springReact.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@org.springframework.stereotype.Controller
public class Controller {

	@GetMapping(value="/")
	public ModelAndView getHome() {
		ModelAndView model =new ModelAndView();
		
		model.setViewName("index");

		return model;

	}
	
}
