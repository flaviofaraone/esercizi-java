package it.softwareInside.springReact.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@AllArgsConstructor
@Entity
@NoArgsConstructor
@Data
@Table(name="persona")
public class Persona {
	
	@Id
	@Column(name="codiceFiscale" , length = 10)
	private String codiceFiscale;
	
	@Column(name ="nome")
	private String nome;
	
	
	https://pastebin.com/aSZRqcA0

}
