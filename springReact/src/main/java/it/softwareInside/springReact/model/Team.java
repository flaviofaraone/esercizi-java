package it.softwareInside.springReact.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table
public class Team {
	
	@Id
	@Column(name="name" , length = 10)
	private String name;
	
	@Column
	private int classifica;
	
}
