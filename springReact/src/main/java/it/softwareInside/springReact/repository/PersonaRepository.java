package it.softwareInside.springReact.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import it.softwareInside.springReact.model.Persona;

@Repository
public interface PersonaRepository extends JpaRepository<Persona, String> {

}