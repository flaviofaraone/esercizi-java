package it.softwareInside.springReact.repository;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import it.softwareInside.springReact.model.Team;

@Repository
public interface TeamRepository extends CrudRepository<Team, String>{
	/**
	 *@return squadre con punti in class >=a input
	 */
	@Query(" select c from Team c where c.classifica >= ?1")
	List<Team> findByClassifica(int num);
	
	
	/**
	 * @return le sqadre con pti in class compresi tra inp1 e inp2
	 */
	@Query("select c from Team c where c.classifica >= ?1 and c.classifica <= ?2")
	List<Team> findBetween( int num1 , int num2);
	
	/**
	 * @return la squadra del nome inserito.
	 */
	@Query("select c from Team c where c.name like ?1")
	Team findByName(String nome);


}
