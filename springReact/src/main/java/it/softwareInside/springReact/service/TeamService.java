package it.softwareInside.springReact.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.softwareInside.springReact.model.Team;
import it.softwareInside.springReact.repository.TeamRepository;

@Service
public class TeamService {
	
	@Autowired
	private TeamRepository teamRepository;
	
	public void addTeam (Team team ) {
		teamRepository.save(team);
	}
	
	public void deleteTeam (Team team ) {
		teamRepository.delete(team);
	}
	
	public List<Team> findAll(){
		return (List<Team>) teamRepository.findAll();
	}
	
	public Team findByName(String name) {
		return teamRepository.findByName(name);
	}

}
