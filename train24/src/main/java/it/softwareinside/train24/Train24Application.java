package it.softwareinside.train24;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Train24Application {

	public static void main(String[] args) {
		SpringApplication.run(Train24Application.class, args);
	}

}
