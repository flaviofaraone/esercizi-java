package it.softwareinside.train24.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import it.softwareinside.train24.model.Cialda;
import it.softwareinside.train24.model.MacchinettaCaffe;
import it.softwareinside.train24.repository.CialdaRepository;
import it.softwareinside.train24.service.CialdaService;
import it.softwareinside.train24.service.MacchinettaCaffeService;

@org.springframework.stereotype.Controller
public class Controller {
	
	
	@Autowired
	private MacchinettaCaffeService macchinettaCaffeService;
	
	@Autowired
	private CialdaService cialdaService;
	
	@GetMapping(value="/")
	public ModelAndView index() {
		ModelAndView model = new ModelAndView();
		 model.setViewName("index");
		 model.addObject("macchinettaCaffe" ,  new MacchinettaCaffe());		
		return model;
	}
	
	@PostMapping(value="/salva")
	public String salva(@ModelAttribute(name="macchina") MacchinettaCaffe macchina) {
		
		macchinettaCaffeService.addMacchinetta(macchina);
		
		return "redirect:/";
	}

	
	
	
}
