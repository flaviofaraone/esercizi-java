package it.softwareinside.train24.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name="cialda")
public class Cialda {
	
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	@Column(name="id_cialda")
	private int id_cialda;
	
	@Column(name="miscela")
	private String miscela;

	@Column(name="marca")
	private String marca;
	
	@Column(name="prezzo")
	private double prezzo;

	public Cialda(String miscela, String marca, double prezzo) {
		super();
		this.miscela = miscela;
		this.marca = marca;
		this.prezzo = prezzo;
	}
	
}