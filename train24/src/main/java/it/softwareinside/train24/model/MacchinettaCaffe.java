package it.softwareinside.train24.model;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name="macchinettaCaffe")
public class MacchinettaCaffe {
	
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	@Column(name="id")
	private int id;
	
	@Column(name="marca")
	private String marca;
	
	@Column(name="prezzo")	
	private double prezzo;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="id_cialda")
	@MapsId
	private Cialda cialda;

	public MacchinettaCaffe(String marca, double prezzo, Cialda cialda) {
		super();
		this.marca = marca;
		this.prezzo = prezzo;
		this.cialda = cialda;
	}
}