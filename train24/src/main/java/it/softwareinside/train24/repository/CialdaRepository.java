package it.softwareinside.train24.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.softwareinside.train24.model.Cialda;

@Repository
public interface CialdaRepository 
						extends CrudRepository<Cialda, Integer> {

}
