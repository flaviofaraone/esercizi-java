package it.softwareinside.train24.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.softwareinside.train24.model.MacchinettaCaffe;
/**
 * query
 * findByMarca
 * findByPrezzo  -----> tutte le macchine del caffe con prezzo = o minore.
 * findBetweenPrezzo
 */
@Repository
public interface MacchinettaCaffeRepository  
							extends CrudRepository<MacchinettaCaffe, Integer> {

	@Query("select x from MacchinettaCaffe x where x.marca like ?1")
	List<MacchinettaCaffe> findByMarca(String marca);
	
	@Query("select x from MacchinettaCaffe x where x.prezzo <= ?1")
	List<MacchinettaCaffe> findByPrezzo(double prezzo);
	
	@Query("select x from MacchinettaCaffe x where x.prezzo >= ?1 and x.prezzo <= ?2")
	List<MacchinettaCaffe> findBetweenPrezzo(double min , double max);	
}
