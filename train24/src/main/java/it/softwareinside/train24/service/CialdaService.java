package it.softwareinside.train24.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.softwareinside.train24.model.Cialda;
import it.softwareinside.train24.repository.CialdaRepository;

@Service
public class CialdaService {
	
	@Autowired
	private CialdaRepository cialdaRepository;
	
	public void addCialda(Cialda cialda) {
		cialdaRepository.save(cialda);
	}
	
	public void deleteCialda(Cialda cialda) {
		cialdaRepository.delete(cialda);
	}
	
}
