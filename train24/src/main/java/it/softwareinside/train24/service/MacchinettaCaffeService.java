package it.softwareinside.train24.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.softwareinside.train24.model.Cialda;
import it.softwareinside.train24.model.MacchinettaCaffe;
import it.softwareinside.train24.repository.MacchinettaCaffeRepository;

@Service
public class MacchinettaCaffeService {

	@Autowired
	private MacchinettaCaffeRepository macchinettaCaffeRepository;
	
	public void addMacchinetta (MacchinettaCaffe macchinettaCaffe) {
		macchinettaCaffeRepository.save(macchinettaCaffe);
	}
	
	public void deleteMacchinetta (MacchinettaCaffe macchinettaCaffe) {
		macchinettaCaffeRepository.delete(macchinettaCaffe);
	}
	
	public void deleteIdMacchinetta (Integer id) {
		macchinettaCaffeRepository.deleteById(id);
	}
	
	//----------------------------------------------------------
	
	public List<MacchinettaCaffe> findByMarca(String marca){
		return macchinettaCaffeRepository.findByMarca(marca);
	}
	
	public List<MacchinettaCaffe> findByPrezzo(double prezzo){
		return macchinettaCaffeRepository.findByPrezzo(prezzo);
	}
	
	public List<MacchinettaCaffe> findBetweenPrezzo(int min , int max){
		return macchinettaCaffeRepository.findBetweenPrezzo(min , max);
	}
	
	
	public void generaMacchina(int numeroMacchine) {
		
		for(int i = 0 ; i < numeroMacchine ; i++) {
			MacchinettaCaffe macc = new MacchinettaCaffe("Javazza", 67, 
								 new Cialda("arabi", "jj", 0.44));
			macchinettaCaffeRepository.save(macc);
		}
		
	}
	
}
