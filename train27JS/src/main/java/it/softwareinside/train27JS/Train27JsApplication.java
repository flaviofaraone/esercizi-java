package it.softwareinside.train27JS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Train27JsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Train27JsApplication.class, args);
	}

}
