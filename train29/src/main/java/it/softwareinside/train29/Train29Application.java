package it.softwareinside.train29;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Train29Application {

	public static void main(String[] args) {
		SpringApplication.run(Train29Application.class, args);
	}

}
