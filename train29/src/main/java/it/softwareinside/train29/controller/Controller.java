package it.softwareinside.train29.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import it.softwareinside.train29.service.ServicePersonaX;

@org.springframework.stereotype.Controller
public class Controller {
	
	@Autowired
	private ServicePersonaX personaXService;

	@GetMapping(value="/")
	public ModelAndView index() {
		ModelAndView model= new ModelAndView();
		model.setViewName("index");
			
		return model;
	}
	
	@GetMapping(value="/persone")
	public ModelAndView persone() {
		ModelAndView model= new ModelAndView();
		model.setViewName("persone");
		
		model.addObject("listaPersone", personaXService.getAllPersone());
			
		return model;
	}
	
	@GetMapping(value="/cancellaPersona")
	public String cancella(@RequestParam(name="id") int id) {
		
		personaXService.deleteById(id);
		
		return "redirect=/persone";
	}
	
	
	@GetMapping(value="/crea")
	public String crea() {
		
		personaXService.riempiPersona(5);
		
		return "redirect=/";
	}
	
}
