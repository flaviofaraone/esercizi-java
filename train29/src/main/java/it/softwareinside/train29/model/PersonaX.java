package it.softwareinside.train29.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@Table
public class PersonaX {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name="nome")
	private String nome;
	
//	@Column(name="cognome")
//	private String cognome;
//	
	@Column(name="eta")
	private int eta;

	public PersonaX(String nome, int eta) {
		super();
		this.nome = nome;
		this.eta = eta;
	}
	
}
