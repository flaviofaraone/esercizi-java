package it.softwareinside.train29.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.softwareinside.train29.model.PersonaX;

@Repository
public interface PersonaXRepository extends CrudRepository<PersonaX, Integer> {

}
