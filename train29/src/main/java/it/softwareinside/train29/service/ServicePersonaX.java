package it.softwareinside.train29.service;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.softwareinside.train29.model.PersonaX;
import it.softwareinside.train29.repository.PersonaXRepository;

@Service
public class ServicePersonaX {
	
	@Autowired
	private PersonaXRepository personaXRepository;
	
	public void getPersonaById(int id) {
//		personaXRepository.findAllById( ).get();
	}
		
	public void addPersonaX(PersonaX personaX) {
		personaXRepository.save(personaX);
	}
	
	public List<PersonaX> getAllPersone(){
		return (List<PersonaX>) personaXRepository.findAll();
	}
	
	public void deleteById(int id) {
		personaXRepository.deleteById(id);
	}
	
	public void deletePerosna(PersonaX personaX) {
		personaXRepository.delete(personaX);
	}
	
	public void modificaPersona(PersonaX personaX) {
		personaXRepository.save(personaX);
	}
	
	public void riempiPersona(int numeroPersone) {
		
		for(int i =0 ; i < numeroPersone; i++) {
			
			String[] names= {"giuseppe" , "dossi" , "marco" , "danilo"};
			String nome = names[new Random().nextInt(names.length)];
			
			PersonaX p1= new PersonaX(nome , 20);
			
			personaXRepository.save(p1);
		}
		
	}
	
	
	
	
	
	
}
