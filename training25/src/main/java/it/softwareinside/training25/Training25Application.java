package it.softwareinside.training25;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Training25Application {

	public static void main(String[] args) {
		SpringApplication.run(Training25Application.class, args);
	}

}
