package it.softwareinside.training25.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import it.softwareinside.training25.model.Auto;
import it.softwareinside.training25.model.Pilota;
import it.softwareinside.training25.service.AutoService;
import it.softwareinside.training25.service.PilosaService;

@org.springframework.stereotype.Controller
public class Controller {

	@Autowired
	private AutoService autoservice;
	
	@Autowired
	private PilosaService pilotaservice;
	
	@GetMapping(value="/")
	public ModelAndView index() {
		ModelAndView model = new ModelAndView();
		 model.setViewName("index");
		 model.addObject("auto" , new Auto());
		 
		 model.addObject("automobili" , autoservice.getAllAuto());
//		 model.addObject(pilotaservice.getAllPiloti());
		 
		 autoservice.run();
		 model.addObject("autoVincente" , autoservice.run());
		 
		 return model;
	}
	
	@PostMapping(value="/salva")
	public String salva (@ModelAttribute(name="auto") Auto auto) {
		
		autoservice.addAuto(auto);
		return "redirect:/";
	}
	
	
	
}