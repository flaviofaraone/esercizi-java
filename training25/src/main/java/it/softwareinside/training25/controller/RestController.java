package it.softwareinside.training25.controller;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import it.softwareinside.training25.model.Auto;
import it.softwareinside.training25.service.AutoService;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api")
public class RestController {
	
	@Autowired
	private AutoService autoservice;
	
	
	@GetMapping("/automobili")
	public ArrayList<Auto> tutteLeAuto(){
		
	return (ArrayList<Auto>) autoservice.getAllAuto();
				
	}
	
	@GetMapping("/findByTeam")
	public List<Auto> findByTeam(@RequestParam(name="team") String team){
		
		
		return autoservice.findByTeam(team);
	}
	
	
}
