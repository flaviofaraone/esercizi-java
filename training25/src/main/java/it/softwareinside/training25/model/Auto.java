package it.softwareinside.training25.model;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name="auto")
public class Auto {
	
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO )
	@Column
	private int id_auto;
	
	@Column(name="team")
	private String team;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="id_auto")
	@MapsId
	private Pilota pilota;

	public Auto(String team, Pilota pilota) {
		super();
		this.team = team;
		this.pilota = pilota;
	}
	
}