package it.softwareinside.training25.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@Entity
@Table(name="pilota")
public class Pilota {
		
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO)
	@Column(name="id")
	private int id;
	
	@Column(name="cognome")
	private String cognome;
	
	@Column(name="esperienza")
	private int esperienza;

	public Pilota(String cognome, int esperienza) {
		super();
		this.cognome = cognome;
		this.esperienza = esperienza;
	}

}