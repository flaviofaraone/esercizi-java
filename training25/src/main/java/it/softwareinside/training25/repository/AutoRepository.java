package it.softwareinside.training25.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.softwareinside.training25.model.Auto;
@Repository
public interface AutoRepository extends CrudRepository<Auto, Integer>{

	@Query("select c from Auto c where c.team like ?1")
	List<Auto> findByTeam(String team);
	
}
