package it.softwareinside.training25.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import it.softwareinside.training25.model.Pilota;
@Repository
public interface PilotaRepository extends CrudRepository<Pilota, Integer> {
	
	
	
}
