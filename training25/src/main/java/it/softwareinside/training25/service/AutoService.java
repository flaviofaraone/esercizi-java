package it.softwareinside.training25.service;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import it.softwareinside.training25.model.Auto;
import it.softwareinside.training25.repository.AutoRepository;
@Service
public class AutoService {

	@Autowired
	private AutoRepository autoRepository;
	
	
	public void addAuto(Auto auto) {
		autoRepository.save(auto);
	}
	
	public void deleteAuto(Auto auto) {
		autoRepository.delete(auto);
	}
	
	public void deleteByIdAuto(int id) {
		autoRepository.deleteById(id);
	}
	
	public List<Auto> getAllAuto(){
		return(List<Auto>) autoRepository.findAll();		
	}
	
	
	public Auto run() {
		
		List<Auto> listaAuto = getAllAuto();
		
		Random rand = new Random();
	    Auto auto =	listaAuto.get(rand.nextInt(listaAuto.size()));
		
	    return auto;
	}
	
	public List<Auto> findByTeam(String team) {
		return autoRepository.findByTeam(team);
	}
	

	
}
