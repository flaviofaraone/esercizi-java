package it.softwareinside.training25.service;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import it.softwareinside.training25.model.Pilota;
import it.softwareinside.training25.repository.PilotaRepository;
@Service
public class PilosaService {

	@Autowired
	private PilotaRepository pilotaRepository;
	
	public void addPilota(Pilota pilota) {
		pilotaRepository.save(pilota);
	}
	
	public void deletePilota(Pilota pilota) {
		pilotaRepository.delete(pilota);
	}
	
	public void deleteByIdPilota(int id) {
		pilotaRepository.deleteById(id);
	}
		
	public List<Pilota> getAllPiloti(){
		return(List<Pilota>) pilotaRepository.findAll();		
	}
}