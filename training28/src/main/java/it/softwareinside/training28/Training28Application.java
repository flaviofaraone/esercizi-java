package it.softwareinside.training28;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Training28Application {

	public static void main(String[] args) {
		SpringApplication.run(Training28Application.class, args);
	}

}
