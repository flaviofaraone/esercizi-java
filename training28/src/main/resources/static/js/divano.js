class Divano{
    constructor(colore , dimensione , posti , isDivanoLetto){
	this.colore=colore ;
	this.dimensione=dimensione;
	this.posti=posti; 
	this.isDivanoLetto=isDivanoLetto;
    }

}

class Dimensione{
	constructor(lunghezza , larghezza){
		this.lunghezza=lunghezza;
		this.larghezza=larghezza;
	}
}